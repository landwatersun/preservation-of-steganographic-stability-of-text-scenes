﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using System.IO;

namespace nist
{
    public class NotFindTemplates : Exception
    { }

    class Program
    {
        const double ALPHA = 0.01;      //p_value < ALPHA ? "FAILURE" : "SUCCESS"
        const int APPROXIMATE_M = 10;   //10 
        const int COMPLEXITY_M = 512;   //500
        const int FREQUENCY_M = 128;    //128
        const int NONOVERLAPPING_M = 9; //9
        const int OVERLAPPING_M = 9;    //9
        const int SERIAL_M = 16;        //16

        [DllImport("nist.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.Cdecl)]
        static extern int NistLoadFile([MarshalAs(UnmanagedType.LPStr)]String lpFileName, int Size);
        [DllImport("nist.dll", CallingConvention = CallingConvention.Cdecl)]
        static extern double NistFrequency();
        [DllImport("nist.dll", CallingConvention = CallingConvention.Cdecl)]
        static extern double NistBlockFrequency(int M);
        [DllImport("nist.dll", CallingConvention = CallingConvention.Cdecl)]
        static extern double NistCumulativeSums();
        [DllImport("nist.dll", CallingConvention = CallingConvention.Cdecl)]
        static extern double NistRuns();
        [DllImport("nist.dll", CallingConvention = CallingConvention.Cdecl)]
        static extern double NistLongestRunOfOnes();
        [DllImport("nist.dll", CallingConvention = CallingConvention.Cdecl)]
        static extern double NistRank();
        [DllImport("nist.dll", CallingConvention = CallingConvention.Cdecl)]
        static extern double NistDiscreteFourierTransform();
        [DllImport("nist.dll", CallingConvention = CallingConvention.Cdecl)]
        static extern double NistNonOverlappingTemplateMatchings(int M);
        [DllImport("nist.dll", CallingConvention = CallingConvention.Cdecl)]
        static extern double NistOverlappingTemplateMatchings(int M);
        [DllImport("nist.dll", CallingConvention = CallingConvention.Cdecl)]
        static extern double NistUniversal();
        [DllImport("nist.dll", CallingConvention = CallingConvention.Cdecl)]
        static extern double NistApproximateEntropy(int M);
        [DllImport("nist.dll", CallingConvention = CallingConvention.Cdecl)]
        static extern double NistRandomExcursions();
        [DllImport("nist.dll", CallingConvention = CallingConvention.Cdecl)]
        static extern double NistRandomExcursionsVariant();
        [DllImport("nist.dll", CallingConvention = CallingConvention.Cdecl)]
        static extern double NistLinearComplexity(int M);
        [DllImport("nist.dll", CallingConvention = CallingConvention.Cdecl)]
        static extern double NistSerial(int M);
        [DllImport("nist.dll", CallingConvention = CallingConvention.Cdecl)]
        static extern int NistCleanup();

        static void Main(string[] args)
        {
            try
            {
                if (!Directory.Exists("templates")) throw new NotFindTemplates();
                StreamWriter log = new StreamWriter(new FileStream("log-" + args[0]+".txt", FileMode.Create));
                int ret = NistLoadFile(args[0], (int)new FileInfo(args[0]).Length);
                char[] tests = new char[15];
                double p = 0;
                int k = 0;
                if (ret != -1)
                {
                    p = NistFrequency();
                    tests[k++] = p < ALPHA ? '0' : '1';
                    p = NistBlockFrequency(FREQUENCY_M);
                    tests[k++] = p < ALPHA ? '0' : '1';
                    p = NistCumulativeSums();
                    tests[k++] = p < ALPHA ? '0' : '1';
                    p = NistRuns();
                    tests[k++] = p < ALPHA ? '0' : '1';
                    p = NistLongestRunOfOnes();
                    tests[k++] = p < ALPHA ? '0' : '1';
                    p = NistRank();
                    tests[k++] = p < ALPHA ? '0' : '1';
                    p = NistDiscreteFourierTransform();
                    tests[k++] = p < ALPHA ? '0' : '1';
                    p = NistNonOverlappingTemplateMatchings(NONOVERLAPPING_M);
                    tests[k++] = p < ALPHA ? '0' : '1';
                    p = NistOverlappingTemplateMatchings(OVERLAPPING_M);
                    tests[k++] = p < ALPHA ? '0' : '1';
                    p = NistUniversal();
                    tests[k++] = p < ALPHA ? '0' : '1';
                    p = NistApproximateEntropy(APPROXIMATE_M);
                    tests[k++] = p < ALPHA ? '0' : '1';
                    p = NistRandomExcursions();
                    tests[k++] = p < ALPHA ? '0' : '1';
                    p = NistRandomExcursionsVariant();
                    tests[k++] = p < ALPHA ? '0' : '1';
                    p = NistLinearComplexity(COMPLEXITY_M);
                    tests[k++] = p < ALPHA ? '0' : '1';
                    p = NistSerial(SERIAL_M);
                    tests[k++] = p < ALPHA ? '0' : '1';
                }
                log.Write(new string(tests));
                log.Close();
                NistCleanup();
            }
            catch (NotFindTemplates)
            {
                Console.WriteLine("Отсутствует папка \"templates\" для выполнения теста \"NistNonOverlappingTemplateMatchings\".");
            }
            catch (Exception exc)
            {
                Console.WriteLine(exc.ToString());
            }
        }
    }
}