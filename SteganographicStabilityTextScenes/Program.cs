﻿using System;
using System.IO;
using Meisui.Random;
using Stego;
using System.Threading.Tasks;
using System.Diagnostics;

namespace SteganographicStabilityTextScenes
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                const int np = 4; // количество 12-поточных запусков: эмуляция np*12 потоков
                // const int nn = 40;
                const int nn = 30;
                const int n = 9 * nn - 12; // 9 * 60 - 12 = 528 | 9 * 40 - 12 = 348 | 9 * 30 - 12 = 258
                // const int CountStego = 1800 * 9;
                const int CountStego = 1808 * 9; // при nn = 30
                const int N = CountStego * n / 32;
                const int NumExper = 1000;
                Stegomask s = new Stegomask(nn);
                char[,] key;
                char[,] etalons;
                s.GetEtalons(out etalons);

                double TotalSuccess = 0;

                StreamWriter sw = new StreamWriter(new FileStream("log-failure.txt", FileMode.Create));

                for (int kl = 0; kl < NumExper; kl++)
                {
                    s.GenerateKey();
                    s.GetKey(out key);

                    Random rand = new Random(Guid.NewGuid().GetHashCode());
                    uint[] messages = new uint[CountStego];
                    for (int i = 0; i < messages.Length; i++)
                        messages[i] = (uint)rand.Next(10);

                    int failure = 0;

                    for (int p = 0; p < np; p++)
                    {
                        Task<string>[] tasks = new Task<string>[12];

                        for (int t = 0; t < 12; t++)
                        {
                            int t_copy = t;
                            tasks[t] = Task<string>.Factory.StartNew(() =>
                            {
                                char[] gamma = new char[CountStego * n];
                                MersenneTwister r = new MersenneTwister();
                                for (int i = 0; i < N; i++)
                                    Convert.ToString(r.genrand_Int32(), 2).PadLeft(32, '0').CopyTo(0, gamma, i * 32, 32);

                                for (int i = 0; i < CountStego; i++)
                                    for (int k = 0; k < n; k++)
                                        if (key[messages[i], k] != '0')
                                            gamma[i * n + k] = etalons[messages[i], k];

                                string stego = new string(gamma);
                                BinaryWriter stegoFile = new BinaryWriter(new FileStream($"stego{t_copy}.bin", FileMode.Create));
                                for (int i = 0; i < N; i++)
                                    stegoFile.Write(Convert.ToUInt32(stego.Substring(i * 32, 32), 2));
                                stegoFile.Close();

                                var startInfo = new ProcessStartInfo
                                {
                                    FileName = "nist.exe",
                                    UseShellExecute = false,
                                    CreateNoWindow = true,
                                    Arguments = $"stego{t_copy}.bin"
                                };
                                Process proc = Process.Start(startInfo);
                                proc.WaitForExit();
                                StreamReader sr = new StreamReader(new FileStream($"log-stego{t_copy}.bin.txt", FileMode.Open));
                                bool nist = sr.ReadLine().Contains("0");
                                sr.Close();
                                File.Delete($"stego{t_copy}.bin");
                                File.Delete($"log-stego{t_copy}.bin.txt");
                                if (nist)
                                    return "FAILURE";
                                else return "SUCCESS";
                            }, TaskCreationOptions.LongRunning);
                        }
                        string[] results = Task.WhenAll(tasks).Result;
                        foreach (string f in results)
                            if (f == "FAILURE") failure++;
                    }
                    sw.WriteLine(failure);
                    Console.WriteLine(failure);
                    if (failure < np * 12)
                        TotalSuccess++;
                }
                Console.WriteLine($"Average success: {TotalSuccess / NumExper}.");
                sw.WriteLine($"Average success: {TotalSuccess / NumExper}.");
                sw.Close();
            }
            catch (WrongSize)
            {
                Console.WriteLine("Ошибка: m объекта должен быть не менее 11.");
            }
            catch (Exception exc)
            {
                Console.WriteLine(exc.ToString());
            }
        }
    }
}
