﻿namespace Drbg_Test
{
    partial class frmNist
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grpAlgorithm = new System.Windows.Forms.GroupBox();
            this.lvAlgorithmTest = new System.Windows.Forms.ListView();
            this.col1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.col2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.col3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.panel2 = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.rd10Mib = new System.Windows.Forms.RadioButton();
            this.rd1Mib = new System.Windows.Forms.RadioButton();
            this.rd100Kib = new System.Windows.Forms.RadioButton();
            this.rd10Kib = new System.Windows.Forms.RadioButton();
            this.btnAnalyze = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.rdRandOrg = new System.Windows.Forms.RadioButton();
            this.label1 = new System.Windows.Forms.Label();
            this.rdCTRSP800DRBG = new System.Windows.Forms.RadioButton();
            this.rdRng = new System.Windows.Forms.RadioButton();
            this.rdCSRG = new System.Windows.Forms.RadioButton();
            this.grpCompare = new System.Windows.Forms.GroupBox();
            this.lvCompare = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader7 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lblOpponent = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.btnCompare = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.rd800Compare = new System.Windows.Forms.RadioButton();
            this.rdRngCompare = new System.Windows.Forms.RadioButton();
            this.btnDialog = new System.Windows.Forms.Button();
            this.txtOutput = new System.Windows.Forms.TextBox();
            this.grpOutput = new System.Windows.Forms.GroupBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.cbDifferential = new System.Windows.Forms.ComboBox();
            this.rdSingle = new System.Windows.Forms.RadioButton();
            this.rdDual = new System.Windows.Forms.RadioButton();
            this.grpTests = new System.Windows.Forms.GroupBox();
            this.OverlappingTemplate = new System.Windows.Forms.CheckBox();
            this.Frequency = new System.Windows.Forms.CheckBox();
            this.RandomExcursionsVariant = new System.Windows.Forms.CheckBox();
            this.RandomExcursions = new System.Windows.Forms.CheckBox();
            this.CumulativeSums = new System.Windows.Forms.CheckBox();
            this.ApproximateEntropy = new System.Windows.Forms.CheckBox();
            this.Serial = new System.Windows.Forms.CheckBox();
            this.LinearComplexity = new System.Windows.Forms.CheckBox();
            this.NonOverlappingTemplate = new System.Windows.Forms.CheckBox();
            this.Universal = new System.Windows.Forms.CheckBox();
            this.Rank = new System.Windows.Forms.CheckBox();
            this.LongestRun = new System.Windows.Forms.CheckBox();
            this.FFT = new System.Windows.Forms.CheckBox();
            this.Runs = new System.Windows.Forms.CheckBox();
            this.BlockFrequency = new System.Windows.Forms.CheckBox();
            this.grpOptions = new System.Windows.Forms.GroupBox();
            this.chkLogResults = new System.Windows.Forms.CheckBox();
            this.lblStatus = new System.Windows.Forms.Label();
            this.pbStatus = new System.Windows.Forms.ProgressBar();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.grpBuild = new System.Windows.Forms.GroupBox();
            this.btnBuild = new System.Windows.Forms.Button();
            this.cbBuildSize = new System.Windows.Forms.ComboBox();
            this.lblBuildSize = new System.Windows.Forms.Label();
            this.grpAlgorithm.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.grpCompare.SuspendLayout();
            this.grpOutput.SuspendLayout();
            this.grpTests.SuspendLayout();
            this.grpOptions.SuspendLayout();
            this.grpBuild.SuspendLayout();
            this.SuspendLayout();
            // 
            // grpAlgorithm
            // 
            this.grpAlgorithm.Controls.Add(this.lvAlgorithmTest);
            this.grpAlgorithm.Controls.Add(this.panel2);
            this.grpAlgorithm.Controls.Add(this.btnAnalyze);
            this.grpAlgorithm.Controls.Add(this.panel1);
            this.grpAlgorithm.Location = new System.Drawing.Point(12, 126);
            this.grpAlgorithm.Name = "grpAlgorithm";
            this.grpAlgorithm.Size = new System.Drawing.Size(491, 300);
            this.grpAlgorithm.TabIndex = 0;
            this.grpAlgorithm.TabStop = false;
            this.grpAlgorithm.Text = "AlgorithmTest";
            // 
            // lvAlgorithmTest
            // 
            this.lvAlgorithmTest.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.col1,
            this.col2,
            this.col3});
            this.lvAlgorithmTest.FullRowSelect = true;
            this.lvAlgorithmTest.GridLines = true;
            this.lvAlgorithmTest.Location = new System.Drawing.Point(9, 22);
            this.lvAlgorithmTest.Name = "lvAlgorithmTest";
            this.lvAlgorithmTest.Size = new System.Drawing.Size(381, 266);
            this.lvAlgorithmTest.TabIndex = 0;
            this.lvAlgorithmTest.UseCompatibleStateImageBehavior = false;
            this.lvAlgorithmTest.View = System.Windows.Forms.View.Details;
            // 
            // col1
            // 
            this.col1.Text = "Test Name";
            this.col1.Width = 139;
            // 
            // col2
            // 
            this.col2.Text = "P Value";
            this.col2.Width = 125;
            // 
            // col3
            // 
            this.col3.Text = "State";
            this.col3.Width = 112;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.rd10Mib);
            this.panel2.Controls.Add(this.rd1Mib);
            this.panel2.Controls.Add(this.rd100Kib);
            this.panel2.Controls.Add(this.rd10Kib);
            this.panel2.Location = new System.Drawing.Point(387, 128);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(101, 112);
            this.panel2.TabIndex = 11;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(6, 4);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(71, 13);
            this.label4.TabIndex = 18;
            this.label4.Text = "Sample Size:";
            // 
            // rd10Mib
            // 
            this.rd10Mib.AutoSize = true;
            this.rd10Mib.Location = new System.Drawing.Point(9, 88);
            this.rd10Mib.Name = "rd10Mib";
            this.rd10Mib.Size = new System.Drawing.Size(54, 17);
            this.rd10Mib.TabIndex = 17;
            this.rd10Mib.Text = "10Mib";
            this.rd10Mib.UseVisualStyleBackColor = true;
            this.rd10Mib.CheckedChanged += new System.EventHandler(this.OnSampleSizeCheckChanged);
            // 
            // rd1Mib
            // 
            this.rd1Mib.AutoSize = true;
            this.rd1Mib.Location = new System.Drawing.Point(9, 65);
            this.rd1Mib.Name = "rd1Mib";
            this.rd1Mib.Size = new System.Drawing.Size(48, 17);
            this.rd1Mib.TabIndex = 16;
            this.rd1Mib.Text = "1Mib";
            this.rd1Mib.UseVisualStyleBackColor = true;
            this.rd1Mib.CheckedChanged += new System.EventHandler(this.OnSampleSizeCheckChanged);
            // 
            // rd100Kib
            // 
            this.rd100Kib.AutoSize = true;
            this.rd100Kib.Checked = true;
            this.rd100Kib.Location = new System.Drawing.Point(9, 42);
            this.rd100Kib.Name = "rd100Kib";
            this.rd100Kib.Size = new System.Drawing.Size(58, 17);
            this.rd100Kib.TabIndex = 15;
            this.rd100Kib.TabStop = true;
            this.rd100Kib.Text = "100Kib";
            this.rd100Kib.UseVisualStyleBackColor = true;
            this.rd100Kib.CheckedChanged += new System.EventHandler(this.OnSampleSizeCheckChanged);
            // 
            // rd10Kib
            // 
            this.rd10Kib.AutoSize = true;
            this.rd10Kib.Location = new System.Drawing.Point(9, 19);
            this.rd10Kib.Name = "rd10Kib";
            this.rd10Kib.Size = new System.Drawing.Size(52, 17);
            this.rd10Kib.TabIndex = 14;
            this.rd10Kib.Text = "10Kib";
            this.rd10Kib.UseVisualStyleBackColor = true;
            this.rd10Kib.CheckedChanged += new System.EventHandler(this.OnSampleSizeCheckChanged);
            // 
            // btnAnalyze
            // 
            this.btnAnalyze.Location = new System.Drawing.Point(407, 255);
            this.btnAnalyze.Name = "btnAnalyze";
            this.btnAnalyze.Size = new System.Drawing.Size(75, 35);
            this.btnAnalyze.TabIndex = 1;
            this.btnAnalyze.Text = "Analyze";
            this.btnAnalyze.UseVisualStyleBackColor = true;
            this.btnAnalyze.Click += new System.EventHandler(this.OnAnalyzeClick);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.rdRandOrg);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.rdCTRSP800DRBG);
            this.panel1.Controls.Add(this.rdRng);
            this.panel1.Controls.Add(this.rdCSRG);
            this.panel1.Location = new System.Drawing.Point(387, 15);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(101, 112);
            this.panel1.TabIndex = 10;
            // 
            // rdRandOrg
            // 
            this.rdRandOrg.AutoSize = true;
            this.rdRandOrg.Location = new System.Drawing.Point(9, 90);
            this.rdRandOrg.Name = "rdRandOrg";
            this.rdRandOrg.Size = new System.Drawing.Size(83, 17);
            this.rdRandOrg.TabIndex = 9;
            this.rdRandOrg.Text = "Random.org";
            this.rdRandOrg.UseVisualStyleBackColor = true;
            this.rdRandOrg.CheckedChanged += new System.EventHandler(this.OnCandidateCheckChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(6, 5);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(63, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "Algorithm:";
            // 
            // rdCTRSP800DRBG
            // 
            this.rdCTRSP800DRBG.AutoSize = true;
            this.rdCTRSP800DRBG.Location = new System.Drawing.Point(9, 67);
            this.rdCTRSP800DRBG.Name = "rdCTRSP800DRBG";
            this.rdCTRSP800DRBG.Size = new System.Drawing.Size(79, 17);
            this.rdCTRSP800DRBG.TabIndex = 7;
            this.rdCTRSP800DRBG.Text = "CTRSP800";
            this.rdCTRSP800DRBG.UseVisualStyleBackColor = true;
            this.rdCTRSP800DRBG.CheckedChanged += new System.EventHandler(this.OnCandidateCheckChanged);
            // 
            // rdRng
            // 
            this.rdRng.AutoSize = true;
            this.rdRng.Location = new System.Drawing.Point(9, 43);
            this.rdRng.Name = "rdRng";
            this.rdRng.Size = new System.Drawing.Size(79, 17);
            this.rdRng.TabIndex = 6;
            this.rdRng.Text = "RNGCrypto";
            this.rdRng.UseVisualStyleBackColor = true;
            this.rdRng.CheckedChanged += new System.EventHandler(this.OnCandidateCheckChanged);
            // 
            // rdCSRG
            // 
            this.rdCSRG.AutoSize = true;
            this.rdCSRG.Checked = true;
            this.rdCSRG.Location = new System.Drawing.Point(9, 20);
            this.rdCSRG.Name = "rdCSRG";
            this.rdCSRG.Size = new System.Drawing.Size(94, 17);
            this.rdCSRG.TabIndex = 5;
            this.rdCSRG.TabStop = true;
            this.rdCSRG.Text = "CSRG (Chaos)";
            this.rdCSRG.UseVisualStyleBackColor = true;
            this.rdCSRG.CheckedChanged += new System.EventHandler(this.OnCandidateCheckChanged);
            // 
            // grpCompare
            // 
            this.grpCompare.Controls.Add(this.lvCompare);
            this.grpCompare.Controls.Add(this.lblOpponent);
            this.grpCompare.Controls.Add(this.label3);
            this.grpCompare.Controls.Add(this.btnCompare);
            this.grpCompare.Controls.Add(this.label2);
            this.grpCompare.Controls.Add(this.rd800Compare);
            this.grpCompare.Controls.Add(this.rdRngCompare);
            this.grpCompare.Location = new System.Drawing.Point(12, 432);
            this.grpCompare.Name = "grpCompare";
            this.grpCompare.Size = new System.Drawing.Size(710, 376);
            this.grpCompare.TabIndex = 1;
            this.grpCompare.TabStop = false;
            this.grpCompare.Text = "Comparative Analysis (10 * 10Kib P-Value Comparison)";
            // 
            // lvCompare
            // 
            this.lvCompare.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3,
            this.columnHeader4,
            this.columnHeader5,
            this.columnHeader6,
            this.columnHeader7});
            this.lvCompare.FullRowSelect = true;
            this.lvCompare.GridLines = true;
            this.lvCompare.Location = new System.Drawing.Point(9, 31);
            this.lvCompare.Name = "lvCompare";
            this.lvCompare.Size = new System.Drawing.Size(692, 284);
            this.lvCompare.TabIndex = 12;
            this.lvCompare.UseCompatibleStateImageBehavior = false;
            this.lvCompare.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Test Name";
            this.columnHeader1.Width = 136;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "P Value Avg.";
            this.columnHeader2.Width = 92;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Low";
            this.columnHeader3.Width = 92;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "High";
            this.columnHeader4.Width = 92;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "P Value Avg.";
            this.columnHeader5.Width = 92;
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "Low";
            this.columnHeader6.Width = 92;
            // 
            // columnHeader7
            // 
            this.columnHeader7.Text = "High";
            this.columnHeader7.Width = 92;
            // 
            // lblOpponent
            // 
            this.lblOpponent.AutoSize = true;
            this.lblOpponent.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOpponent.Location = new System.Drawing.Point(420, 17);
            this.lblOpponent.Name = "lblOpponent";
            this.lblOpponent.Size = new System.Drawing.Size(146, 13);
            this.lblOpponent.TabIndex = 10;
            this.lblOpponent.Text = "RNGCryptoServiceProvider";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(145, 17);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(35, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "CSRG";
            // 
            // btnCompare
            // 
            this.btnCompare.Location = new System.Drawing.Point(626, 330);
            this.btnCompare.Name = "btnCompare";
            this.btnCompare.Size = new System.Drawing.Size(75, 35);
            this.btnCompare.TabIndex = 8;
            this.btnCompare.Text = "Compare";
            this.btnCompare.UseVisualStyleBackColor = true;
            this.btnCompare.Click += new System.EventHandler(this.OnCompareClick);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(9, 328);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(104, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Compare CSRG To:";
            // 
            // rd800Compare
            // 
            this.rd800Compare.AutoSize = true;
            this.rd800Compare.Location = new System.Drawing.Point(280, 326);
            this.rd800Compare.Name = "rd800Compare";
            this.rd800Compare.Size = new System.Drawing.Size(110, 17);
            this.rd800Compare.TabIndex = 6;
            this.rd800Compare.Text = "CTRSP800DRBG";
            this.rd800Compare.UseVisualStyleBackColor = true;
            this.rd800Compare.CheckedChanged += new System.EventHandler(this.OnCompareCheckChanged);
            // 
            // rdRngCompare
            // 
            this.rdRngCompare.AutoSize = true;
            this.rdRngCompare.Checked = true;
            this.rdRngCompare.Location = new System.Drawing.Point(120, 326);
            this.rdRngCompare.Name = "rdRngCompare";
            this.rdRngCompare.Size = new System.Drawing.Size(154, 17);
            this.rdRngCompare.TabIndex = 5;
            this.rdRngCompare.TabStop = true;
            this.rdRngCompare.Text = "RNGCryptoServiceProvider";
            this.rdRngCompare.UseVisualStyleBackColor = true;
            this.rdRngCompare.CheckedChanged += new System.EventHandler(this.OnCompareCheckChanged);
            // 
            // btnDialog
            // 
            this.btnDialog.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDialog.Location = new System.Drawing.Point(260, 18);
            this.btnDialog.Name = "btnDialog";
            this.btnDialog.Size = new System.Drawing.Size(30, 30);
            this.btnDialog.TabIndex = 1;
            this.btnDialog.Text = "...";
            this.btnDialog.UseVisualStyleBackColor = true;
            this.btnDialog.Click += new System.EventHandler(this.OnDialogClick);
            // 
            // txtOutput
            // 
            this.txtOutput.Location = new System.Drawing.Point(6, 24);
            this.txtOutput.Name = "txtOutput";
            this.txtOutput.ReadOnly = true;
            this.txtOutput.Size = new System.Drawing.Size(248, 20);
            this.txtOutput.TabIndex = 2;
            this.txtOutput.Text = "[Select an Output Folder]";
            // 
            // grpOutput
            // 
            this.grpOutput.Controls.Add(this.label6);
            this.grpOutput.Controls.Add(this.label5);
            this.grpOutput.Controls.Add(this.cbDifferential);
            this.grpOutput.Controls.Add(this.rdSingle);
            this.grpOutput.Controls.Add(this.rdDual);
            this.grpOutput.Controls.Add(this.btnDialog);
            this.grpOutput.Controls.Add(this.txtOutput);
            this.grpOutput.Location = new System.Drawing.Point(12, 12);
            this.grpOutput.Name = "grpOutput";
            this.grpOutput.Size = new System.Drawing.Size(299, 108);
            this.grpOutput.TabIndex = 6;
            this.grpOutput.TabStop = false;
            this.grpOutput.Text = "Set the Directory for Tests and Builds";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(145, 56);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(109, 13);
            this.label6.TabIndex = 10;
            this.label6.Text = "ReSeed Differential:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(9, 56);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(103, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "CTR Engine Mode:";
            // 
            // cbDifferential
            // 
            this.cbDifferential.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbDifferential.FormattingEnabled = true;
            this.cbDifferential.Items.AddRange(new object[] {
            "32",
            "64",
            "128",
            "256",
            "512"});
            this.cbDifferential.Location = new System.Drawing.Point(148, 70);
            this.cbDifferential.Name = "cbDifferential";
            this.cbDifferential.Size = new System.Drawing.Size(106, 21);
            this.cbDifferential.TabIndex = 5;
            this.cbDifferential.SelectedValueChanged += new System.EventHandler(this.OnDifferentialChanged);
            // 
            // rdSingle
            // 
            this.rdSingle.AutoSize = true;
            this.rdSingle.Location = new System.Drawing.Point(65, 72);
            this.rdSingle.Name = "rdSingle";
            this.rdSingle.Size = new System.Drawing.Size(54, 17);
            this.rdSingle.TabIndex = 4;
            this.rdSingle.Text = "Single";
            this.rdSingle.UseVisualStyleBackColor = true;
            this.rdSingle.CheckedChanged += new System.EventHandler(this.OnEngineChanged);
            // 
            // rdDual
            // 
            this.rdDual.AutoSize = true;
            this.rdDual.Checked = true;
            this.rdDual.Location = new System.Drawing.Point(12, 72);
            this.rdDual.Name = "rdDual";
            this.rdDual.Size = new System.Drawing.Size(47, 17);
            this.rdDual.TabIndex = 3;
            this.rdDual.TabStop = true;
            this.rdDual.Text = "Dual";
            this.rdDual.UseVisualStyleBackColor = true;
            this.rdDual.CheckedChanged += new System.EventHandler(this.OnEngineChanged);
            // 
            // grpTests
            // 
            this.grpTests.Controls.Add(this.OverlappingTemplate);
            this.grpTests.Controls.Add(this.Frequency);
            this.grpTests.Controls.Add(this.RandomExcursionsVariant);
            this.grpTests.Controls.Add(this.RandomExcursions);
            this.grpTests.Controls.Add(this.CumulativeSums);
            this.grpTests.Controls.Add(this.ApproximateEntropy);
            this.grpTests.Controls.Add(this.Serial);
            this.grpTests.Controls.Add(this.LinearComplexity);
            this.grpTests.Controls.Add(this.NonOverlappingTemplate);
            this.grpTests.Controls.Add(this.Universal);
            this.grpTests.Controls.Add(this.Rank);
            this.grpTests.Controls.Add(this.LongestRun);
            this.grpTests.Controls.Add(this.FFT);
            this.grpTests.Controls.Add(this.Runs);
            this.grpTests.Controls.Add(this.BlockFrequency);
            this.grpTests.Location = new System.Drawing.Point(509, 12);
            this.grpTests.Name = "grpTests";
            this.grpTests.Size = new System.Drawing.Size(213, 364);
            this.grpTests.TabIndex = 7;
            this.grpTests.TabStop = false;
            this.grpTests.Text = "Test Groups";
            // 
            // OverlappingTemplate
            // 
            this.OverlappingTemplate.AutoSize = true;
            this.OverlappingTemplate.Checked = true;
            this.OverlappingTemplate.CheckState = System.Windows.Forms.CheckState.Checked;
            this.OverlappingTemplate.Location = new System.Drawing.Point(9, 226);
            this.OverlappingTemplate.Name = "OverlappingTemplate";
            this.OverlappingTemplate.Size = new System.Drawing.Size(177, 17);
            this.OverlappingTemplate.TabIndex = 84;
            this.OverlappingTemplate.Text = "Overlapping Template Matching";
            this.OverlappingTemplate.UseVisualStyleBackColor = true;
            this.OverlappingTemplate.CheckedChanged += new System.EventHandler(this.OnTestCountChanged);
            // 
            // Frequency
            // 
            this.Frequency.AutoSize = true;
            this.Frequency.Checked = true;
            this.Frequency.CheckState = System.Windows.Forms.CheckState.Checked;
            this.Frequency.Location = new System.Drawing.Point(9, 111);
            this.Frequency.Name = "Frequency";
            this.Frequency.Size = new System.Drawing.Size(117, 17);
            this.Frequency.TabIndex = 69;
            this.Frequency.Text = "Frequency Monobit";
            this.Frequency.UseVisualStyleBackColor = true;
            this.Frequency.CheckedChanged += new System.EventHandler(this.OnTestCountChanged);
            // 
            // RandomExcursionsVariant
            // 
            this.RandomExcursionsVariant.AutoSize = true;
            this.RandomExcursionsVariant.Checked = true;
            this.RandomExcursionsVariant.CheckState = System.Windows.Forms.CheckState.Checked;
            this.RandomExcursionsVariant.Enabled = false;
            this.RandomExcursionsVariant.Location = new System.Drawing.Point(9, 272);
            this.RandomExcursionsVariant.Name = "RandomExcursionsVariant";
            this.RandomExcursionsVariant.Size = new System.Drawing.Size(194, 17);
            this.RandomExcursionsVariant.TabIndex = 83;
            this.RandomExcursionsVariant.Text = "Random Excursions Variant (1Mib+)";
            this.RandomExcursionsVariant.UseVisualStyleBackColor = true;
            this.RandomExcursionsVariant.CheckedChanged += new System.EventHandler(this.OnTestCountChanged);
            // 
            // RandomExcursions
            // 
            this.RandomExcursions.AutoSize = true;
            this.RandomExcursions.Checked = true;
            this.RandomExcursions.CheckState = System.Windows.Forms.CheckState.Checked;
            this.RandomExcursions.Enabled = false;
            this.RandomExcursions.Location = new System.Drawing.Point(9, 249);
            this.RandomExcursions.Name = "RandomExcursions";
            this.RandomExcursions.Size = new System.Drawing.Size(158, 17);
            this.RandomExcursions.TabIndex = 82;
            this.RandomExcursions.Text = "Random Excursions (1Mib+)";
            this.RandomExcursions.UseVisualStyleBackColor = true;
            this.RandomExcursions.CheckedChanged += new System.EventHandler(this.OnTestCountChanged);
            // 
            // CumulativeSums
            // 
            this.CumulativeSums.AutoSize = true;
            this.CumulativeSums.Checked = true;
            this.CumulativeSums.CheckState = System.Windows.Forms.CheckState.Checked;
            this.CumulativeSums.Location = new System.Drawing.Point(9, 65);
            this.CumulativeSums.Name = "CumulativeSums";
            this.CumulativeSums.Size = new System.Drawing.Size(107, 17);
            this.CumulativeSums.TabIndex = 81;
            this.CumulativeSums.Text = "Cumulative Sums";
            this.CumulativeSums.UseVisualStyleBackColor = true;
            this.CumulativeSums.CheckedChanged += new System.EventHandler(this.OnTestCountChanged);
            // 
            // ApproximateEntropy
            // 
            this.ApproximateEntropy.AutoSize = true;
            this.ApproximateEntropy.Checked = true;
            this.ApproximateEntropy.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ApproximateEntropy.Location = new System.Drawing.Point(9, 19);
            this.ApproximateEntropy.Name = "ApproximateEntropy";
            this.ApproximateEntropy.Size = new System.Drawing.Size(123, 17);
            this.ApproximateEntropy.TabIndex = 80;
            this.ApproximateEntropy.Text = "Approximate Entropy";
            this.ApproximateEntropy.UseVisualStyleBackColor = true;
            this.ApproximateEntropy.CheckedChanged += new System.EventHandler(this.OnTestCountChanged);
            // 
            // Serial
            // 
            this.Serial.AutoSize = true;
            this.Serial.Checked = true;
            this.Serial.CheckState = System.Windows.Forms.CheckState.Checked;
            this.Serial.Location = new System.Drawing.Point(9, 318);
            this.Serial.Name = "Serial";
            this.Serial.Size = new System.Drawing.Size(52, 17);
            this.Serial.TabIndex = 79;
            this.Serial.Text = "Serial";
            this.Serial.UseVisualStyleBackColor = true;
            this.Serial.CheckedChanged += new System.EventHandler(this.OnTestCountChanged);
            // 
            // LinearComplexity
            // 
            this.LinearComplexity.AutoSize = true;
            this.LinearComplexity.Checked = true;
            this.LinearComplexity.CheckState = System.Windows.Forms.CheckState.Checked;
            this.LinearComplexity.Location = new System.Drawing.Point(9, 157);
            this.LinearComplexity.Name = "LinearComplexity";
            this.LinearComplexity.Size = new System.Drawing.Size(108, 17);
            this.LinearComplexity.TabIndex = 78;
            this.LinearComplexity.Text = "Linear Complexity";
            this.LinearComplexity.UseVisualStyleBackColor = true;
            this.LinearComplexity.CheckedChanged += new System.EventHandler(this.OnTestCountChanged);
            // 
            // NonOverlappingTemplate
            // 
            this.NonOverlappingTemplate.AutoSize = true;
            this.NonOverlappingTemplate.Checked = true;
            this.NonOverlappingTemplate.CheckState = System.Windows.Forms.CheckState.Checked;
            this.NonOverlappingTemplate.Location = new System.Drawing.Point(9, 203);
            this.NonOverlappingTemplate.Name = "NonOverlappingTemplate";
            this.NonOverlappingTemplate.Size = new System.Drawing.Size(200, 17);
            this.NonOverlappingTemplate.TabIndex = 76;
            this.NonOverlappingTemplate.Text = "Non Overlapping Template Matching";
            this.NonOverlappingTemplate.UseVisualStyleBackColor = true;
            this.NonOverlappingTemplate.CheckedChanged += new System.EventHandler(this.OnTestCountChanged);
            // 
            // Universal
            // 
            this.Universal.AutoSize = true;
            this.Universal.Checked = true;
            this.Universal.CheckState = System.Windows.Forms.CheckState.Checked;
            this.Universal.Enabled = false;
            this.Universal.Location = new System.Drawing.Point(9, 341);
            this.Universal.Name = "Universal";
            this.Universal.Size = new System.Drawing.Size(108, 17);
            this.Universal.TabIndex = 75;
            this.Universal.Text = "Universal (1Mib+)";
            this.Universal.UseVisualStyleBackColor = true;
            this.Universal.CheckedChanged += new System.EventHandler(this.OnTestCountChanged);
            // 
            // Rank
            // 
            this.Rank.AutoSize = true;
            this.Rank.Checked = true;
            this.Rank.CheckState = System.Windows.Forms.CheckState.Checked;
            this.Rank.Location = new System.Drawing.Point(9, 42);
            this.Rank.Name = "Rank";
            this.Rank.Size = new System.Drawing.Size(115, 17);
            this.Rank.TabIndex = 74;
            this.Rank.Text = "Binary Matrix Rank";
            this.Rank.UseVisualStyleBackColor = true;
            this.Rank.CheckedChanged += new System.EventHandler(this.OnTestCountChanged);
            // 
            // LongestRun
            // 
            this.LongestRun.AutoSize = true;
            this.LongestRun.Checked = true;
            this.LongestRun.CheckState = System.Windows.Forms.CheckState.Checked;
            this.LongestRun.Location = new System.Drawing.Point(9, 180);
            this.LongestRun.Name = "LongestRun";
            this.LongestRun.Size = new System.Drawing.Size(177, 17);
            this.LongestRun.TabIndex = 73;
            this.LongestRun.Text = "Longest Run of Ones in a Block";
            this.LongestRun.UseVisualStyleBackColor = true;
            this.LongestRun.CheckedChanged += new System.EventHandler(this.OnTestCountChanged);
            // 
            // FFT
            // 
            this.FFT.AutoSize = true;
            this.FFT.Checked = true;
            this.FFT.CheckState = System.Windows.Forms.CheckState.Checked;
            this.FFT.Location = new System.Drawing.Point(9, 88);
            this.FFT.Name = "FFT";
            this.FFT.Size = new System.Drawing.Size(198, 17);
            this.FFT.TabIndex = 72;
            this.FFT.Text = "Discrete Fourier Transform (Spectral)";
            this.FFT.UseVisualStyleBackColor = true;
            this.FFT.CheckedChanged += new System.EventHandler(this.OnTestCountChanged);
            // 
            // Runs
            // 
            this.Runs.AutoSize = true;
            this.Runs.Checked = true;
            this.Runs.CheckState = System.Windows.Forms.CheckState.Checked;
            this.Runs.Location = new System.Drawing.Point(9, 295);
            this.Runs.Name = "Runs";
            this.Runs.Size = new System.Drawing.Size(51, 17);
            this.Runs.TabIndex = 71;
            this.Runs.Text = "Runs";
            this.Runs.UseVisualStyleBackColor = true;
            this.Runs.CheckedChanged += new System.EventHandler(this.OnTestCountChanged);
            // 
            // BlockFrequency
            // 
            this.BlockFrequency.AutoSize = true;
            this.BlockFrequency.Checked = true;
            this.BlockFrequency.CheckState = System.Windows.Forms.CheckState.Checked;
            this.BlockFrequency.Location = new System.Drawing.Point(9, 134);
            this.BlockFrequency.Name = "BlockFrequency";
            this.BlockFrequency.Size = new System.Drawing.Size(172, 17);
            this.BlockFrequency.TabIndex = 70;
            this.BlockFrequency.Text = "Frequency Test Within a Block";
            this.BlockFrequency.UseVisualStyleBackColor = true;
            this.BlockFrequency.CheckedChanged += new System.EventHandler(this.OnTestCountChanged);
            // 
            // grpOptions
            // 
            this.grpOptions.Controls.Add(this.chkLogResults);
            this.grpOptions.Location = new System.Drawing.Point(509, 382);
            this.grpOptions.Name = "grpOptions";
            this.grpOptions.Size = new System.Drawing.Size(212, 44);
            this.grpOptions.TabIndex = 9;
            this.grpOptions.TabStop = false;
            this.grpOptions.Text = "Options";
            // 
            // chkLogResults
            // 
            this.chkLogResults.AutoSize = true;
            this.chkLogResults.Location = new System.Drawing.Point(9, 17);
            this.chkLogResults.Name = "chkLogResults";
            this.chkLogResults.Size = new System.Drawing.Size(117, 17);
            this.chkLogResults.TabIndex = 0;
            this.chkLogResults.Text = "Save Results to file";
            this.chkLogResults.UseVisualStyleBackColor = true;
            // 
            // lblStatus
            // 
            this.lblStatus.AutoSize = true;
            this.lblStatus.Location = new System.Drawing.Point(214, 825);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(52, 13);
            this.lblStatus.TabIndex = 13;
            this.lblStatus.Text = "Waiting...";
            // 
            // pbStatus
            // 
            this.pbStatus.Location = new System.Drawing.Point(1, 823);
            this.pbStatus.Name = "pbStatus";
            this.pbStatus.Size = new System.Drawing.Size(206, 16);
            this.pbStatus.TabIndex = 11;
            // 
            // statusStrip1
            // 
            this.statusStrip1.Location = new System.Drawing.Point(0, 820);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(735, 22);
            this.statusStrip1.TabIndex = 12;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // grpBuild
            // 
            this.grpBuild.Controls.Add(this.btnBuild);
            this.grpBuild.Controls.Add(this.cbBuildSize);
            this.grpBuild.Location = new System.Drawing.Point(319, 12);
            this.grpBuild.Name = "grpBuild";
            this.grpBuild.Size = new System.Drawing.Size(184, 108);
            this.grpBuild.TabIndex = 14;
            this.grpBuild.TabStop = false;
            this.grpBuild.Text = "Export a CSRG Random file";
            // 
            // btnBuild
            // 
            this.btnBuild.Location = new System.Drawing.Point(100, 63);
            this.btnBuild.Name = "btnBuild";
            this.btnBuild.Size = new System.Drawing.Size(75, 35);
            this.btnBuild.TabIndex = 11;
            this.btnBuild.Text = "Build";
            this.btnBuild.UseVisualStyleBackColor = true;
            this.btnBuild.Click += new System.EventHandler(this.OnBuildClick);
            // 
            // cbBuildSize
            // 
            this.cbBuildSize.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbBuildSize.FormattingEnabled = true;
            this.cbBuildSize.Items.AddRange(new object[] {
            "10Kib (10240b)",
            "100Kib (102400)",
            "1Mib (1024000)",
            "10Mib (10240000)"});
            this.cbBuildSize.Location = new System.Drawing.Point(10, 24);
            this.cbBuildSize.Name = "cbBuildSize";
            this.cbBuildSize.Size = new System.Drawing.Size(165, 21);
            this.cbBuildSize.TabIndex = 10;
            this.cbBuildSize.SelectedValueChanged += new System.EventHandler(this.OnExportValueChanged);
            // 
            // lblBuildSize
            // 
            this.lblBuildSize.AutoSize = true;
            this.lblBuildSize.Location = new System.Drawing.Point(441, 825);
            this.lblBuildSize.Name = "lblBuildSize";
            this.lblBuildSize.Size = new System.Drawing.Size(16, 13);
            this.lblBuildSize.TabIndex = 15;
            this.lblBuildSize.Text = "...";
            // 
            // frmNist
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(735, 842);
            this.Controls.Add(this.lblBuildSize);
            this.Controls.Add(this.grpBuild);
            this.Controls.Add(this.grpOutput);
            this.Controls.Add(this.lblStatus);
            this.Controls.Add(this.pbStatus);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.grpOptions);
            this.Controls.Add(this.grpTests);
            this.Controls.Add(this.grpAlgorithm);
            this.Controls.Add(this.grpCompare);
            this.HelpButton = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmNist";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "CSRG Test Bed: (Chaotic Random State Generator)";
            this.HelpButtonClicked += new System.ComponentModel.CancelEventHandler(this.OnFormHelpClicked);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.OnFormClose);
            this.Load += new System.EventHandler(this.OnFormLoad);
            this.grpAlgorithm.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.grpCompare.ResumeLayout(false);
            this.grpCompare.PerformLayout();
            this.grpOutput.ResumeLayout(false);
            this.grpOutput.PerformLayout();
            this.grpTests.ResumeLayout(false);
            this.grpTests.PerformLayout();
            this.grpOptions.ResumeLayout(false);
            this.grpOptions.PerformLayout();
            this.grpBuild.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox grpAlgorithm;
        private System.Windows.Forms.GroupBox grpCompare;
        private System.Windows.Forms.Button btnAnalyze;
        private System.Windows.Forms.ListView lvAlgorithmTest;
        private System.Windows.Forms.ColumnHeader col1;
        private System.Windows.Forms.ColumnHeader col2;
        private System.Windows.Forms.Button btnDialog;
        private System.Windows.Forms.TextBox txtOutput;
        private System.Windows.Forms.GroupBox grpOutput;
        private System.Windows.Forms.GroupBox grpTests;
        private System.Windows.Forms.CheckBox RandomExcursionsVariant;
        private System.Windows.Forms.CheckBox RandomExcursions;
        private System.Windows.Forms.CheckBox CumulativeSums;
        private System.Windows.Forms.CheckBox ApproximateEntropy;
        private System.Windows.Forms.CheckBox Serial;
        private System.Windows.Forms.CheckBox LinearComplexity;
        private System.Windows.Forms.CheckBox NonOverlappingTemplate;
        private System.Windows.Forms.CheckBox Universal;
        private System.Windows.Forms.CheckBox Rank;
        private System.Windows.Forms.CheckBox LongestRun;
        private System.Windows.Forms.CheckBox FFT;
        private System.Windows.Forms.CheckBox Runs;
        private System.Windows.Forms.CheckBox BlockFrequency;
        private System.Windows.Forms.CheckBox Frequency;
        private System.Windows.Forms.Label lblOpponent;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnCompare;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.RadioButton rd800Compare;
        private System.Windows.Forms.RadioButton rdRngCompare;
        private System.Windows.Forms.GroupBox grpOptions;
        private System.Windows.Forms.CheckBox chkLogResults;
        private System.Windows.Forms.Label lblStatus;
        private System.Windows.Forms.ProgressBar pbStatus;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ColumnHeader col3;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RadioButton rdCTRSP800DRBG;
        private System.Windows.Forms.RadioButton rdRng;
        private System.Windows.Forms.RadioButton rdCSRG;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.RadioButton rd10Mib;
        private System.Windows.Forms.RadioButton rd1Mib;
        private System.Windows.Forms.RadioButton rd100Kib;
        private System.Windows.Forms.RadioButton rd10Kib;
        private System.Windows.Forms.ListView lvCompare;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.ColumnHeader columnHeader7;
        private System.Windows.Forms.CheckBox OverlappingTemplate;
        private System.Windows.Forms.RadioButton rdRandOrg;
        private System.Windows.Forms.GroupBox grpBuild;
        private System.Windows.Forms.Button btnBuild;
        private System.Windows.Forms.ComboBox cbBuildSize;
        private System.Windows.Forms.Label lblBuildSize;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox cbDifferential;
        private System.Windows.Forms.RadioButton rdSingle;
        private System.Windows.Forms.RadioButton rdDual;
    }
}