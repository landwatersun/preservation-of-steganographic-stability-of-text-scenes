﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security.Cryptography;
using System.Windows.Forms;

namespace Drbg_Test
{
    public partial class Form1 : Form
    {
        #region Constants
        private const string RESULTS_NAME = "results.txt";
        #endregion

        #region Enums
        private enum ApiTypes
        {
            Aes,
            Drbg,
            Rng,
        }

        private enum TestTypes : int
        {
            Entropy = 0,
            ChiSquare,
            ChiProbability,
            Mean,
            MonteCarloErrorPct,
            MonteCarloPiCalc,
            SerialCorrelation,
        }
        #endregion

        #region Fields
        private int Iterations = 1000;
        private int SampleSize = 102400;
        private string _dlgPath = string.Empty;
        private StreamWriter _dataWriter;
        private RNGCryptoServiceProvider _rngRandom = new RNGCryptoServiceProvider();
        private ApiTypes _Opponent = ApiTypes.Rng;
        private Stopwatch _runTimer = new Stopwatch();
        private double _milliSecondsAes = 0;
        private double _milliSecondsOpponent = 0;

        #endregion

        #region Constructor
        public Form1()
        {
            InitializeComponent();

            ////////////////
            // Nist Tests //
            ////////////////
            Tests();

            btnTestAesCtr.Enabled = false;
            btnSaveAlgo.Enabled = false;
            txtOutput.Text = "[Select a Destination Folder]";
            // compare the sha256 implementations with SHA256Managed
            CompareSHA();
            CompareInternalSHA();
            // test output equality
            CompareEqual();
            // test methods (throws on exception, output to debug window)
            TestOutputs();
            // test speed between algorithms (output to debug window)
            CompareSpeed(1024000);
        }

        /// <summary>
        /// Run builds, grab files from Nist, and collect statistics
        /// Careful when using real random, pulls directly from random.org
        /// They have a max quota limit of 1,000,000 bits per day! http://www.random.org/quota
        /// </summary>
        private void Tests()
        {
            // modifiable //
            const string BUILD_PATH = @"C:\Build\";
            const string DATA_PATH = @"C:\Data\";
            // !put your path here!
            const string NIST_PATH = @"C:\Tests\Nist\"; 
            // sub-directory structure
            const string CSRG10K = @"CSRG\10K\";
            const string CSRG100K = @"CSRG\100K\";
            const string CSRG1M = @"CSRG\1M\";
            const string CSRG10M = @"CSRG\10M\";
            const string DRBG80010K = @"800\10K\";
            const string DRBG800100K = @"800\100K\";
            const string RORG10K = @"ROrg\10K\";
            const string RORG100K = @"ROrg\100K\";

            /////////////////////////////////////////////////////////
            // Instructions:                                       //
            // 1. Build a sample or a group of samples             //
            // 2. Grab the files from nist to your test directory  //
            // 3. Collect statistics p-value(low, high, avg)       //
            // 4. Compare them (read)                              //
            // BUILD_PATH - BUILD DIRECTORY                        //
            // DATA_PATH - PUT RESULTS HERE                        //
            // NIST_PATH - ROOT FOLDER OF NIST ex. c:\nist\        //
            // Subdirectories are built automatically              //
            /////////////////////////////////////////////////////////

            // SAMPLE BUILDS //
            //TestBuild.CSRGBuild10KB(BUILD_PATH);
            //TestBuild.DRBG800Build10KB(BUILD_PATH);
            //TestBuild.ROrgBuild10KB(BUILD_PATH);

            //TestBuild.CSRGBuild100KB(BUILD_PATH);
            //TestBuild.DRBG800Build100KB(BUILD_PATH);
            //TestBuild.ROrgBuild100KB(BUILD_PATH);

            // build 10 * 10Kib blocks of CSRG, 800DRBG, and true random from random.org
            //TestBuild.Build10KBx10(BUILD_PATH);
            // build 10 * 100Kib blocks of CSRG, 800DRBG, and true random from random.org
            //TestBuild.Build100KBx10(BUILD_PATH);

            // build 1Mib with CSRG
            //TestBuild.CSRGBuild1MB(BUILD_PATH + CSRG1M);
            // build 10Mib with CSRG
            //TestBuild.CSRGBuild10MB(BUILD_PATH + CSRG10M);
            // build 10 * 10Mib with CSRG
            //TestBuild.CSRGBuild10MBx10(BUILD_PATH + CSRG10M);

            // FILE GRABS //
            // collect stats.txt files from  a Nist test of types: CSRG/DRBG800/RORG
            //TestBuild.CollectNistStats(NIST_PATH, DATA_PATH + CSRG100K);
            //TestBuild.CollectNistStats(NIST_PATH, DATA_PATH + DRBG800100K);
            //TestBuild.CollectNistStats(NIST_PATH, DATA_PATH + RORG100K);

            // collect result.txt files from  a Nist test
            //TestBuild.CollectNistResults(NIST_PATH, DATA_PATH + CSRG100K);
            //TestBuild.CollectNistResults(NIST_PATH, DATA_PATH + DRBG800100K);
            //TestBuild.CollectNistResults(NIST_PATH, DATA_PATH + RORG100K);

            // CALCULATE AVERAGES //
            // Estimate the averages on 10 * 10kib samples for comparison
            //TestBuild.EstimateNistAverages(DATA_PATH + CSRG10K, DATA_PATH + CSRG10K);
            //TestBuild.EstimateNistAverages(DATA_PATH + DRBG80010K, DATA_PATH + DRBG80010K);
            //TestBuild.EstimateNistAverages(DATA_PATH + RORG10K, DATA_PATH + RORG10K);

            // Estimate the averages on 10 * 100kib samples for comparison
            //TestBuild.EstimateNistAverages(DATA_PATH + CSRG100K, DATA_PATH + CSRG100K);
            //TestBuild.EstimateNistAverages(DATA_PATH + DRBG800100K, DATA_PATH + DRBG800100K);
            //TestBuild.EstimateNistAverages(DATA_PATH + RORG100K, DATA_PATH + RORG100K);

            // state change frequency test
            //GetAvgTransitions();
        }
        #endregion

        #region Controls
        private void OnCheckChanged(object sender, EventArgs e)
        {
            if (File.Exists(txtOutput.Text))
                File.Delete(txtOutput.Text);
            if (Directory.Exists(Path.GetDirectoryName(txtOutput.Text)))
            {
                btnTestAesCtr.Enabled = true;
                btnSaveAlgo.Enabled = true;
            }
        }

        private void OnDialogClick(object sender, EventArgs e)
        {
            btnTestAesCtr.Enabled = false;
            btnSaveAlgo.Enabled = false;
            txtOutput.Text = "[Select a Destination Folder]";
            pbStatus.Value = 0;
            lblStatAes.Text = "....";
            lblStatOpponent.Text = "....";

            using (FolderBrowserDialog fbDiag = new FolderBrowserDialog())
            {
                fbDiag.Description = "Select a Folder";

                if (fbDiag.ShowDialog() == DialogResult.OK)
                {
                    if (Directory.Exists(fbDiag.SelectedPath))
                        _dlgPath = Path.Combine(fbDiag.SelectedPath, RESULTS_NAME);
                    if (File.Exists(_dlgPath))
                        File.Delete(_dlgPath);

                    txtOutput.Text = _dlgPath;
                    btnTestAesCtr.Enabled = true;
                    btnSaveAlgo.Enabled = true;
                }
            }
        }

        private void OnOpenFileClick(object sender, EventArgs e)
        {
            string fileName = txtOutput.Text;

            if (File.Exists(fileName))
                Process.Start("notepad.exe", fileName);
        }

        private void OnSaveAlgoClick(object sender, EventArgs e)
        {
            const int Mib = 1024000;
            int size = 10;
            int.TryParse(cbAlgoSize.Text, out size);
            if (size == 0) size = 10;
            if (!Directory.Exists(Path.GetDirectoryName(txtOutput.Text))) return;
            string filePath = Path.Combine(Path.GetDirectoryName(txtOutput.Text), cbAlgo.Text + ".bin");

            for (int i = 0; i < 100; i++)
            {
                if (File.Exists(filePath))
                    filePath = Path.Combine(Path.GetDirectoryName(txtOutput.Text), cbAlgo.Text + i.ToString() + ".bin");
                else
                    break;
            }

            size *= Mib;
            lblStatAes.Text = "Writing to file..";
            lblStatAes.Update();

            TimerStart();

            if (cbAlgo.Text.Equals("AutoSeed"))
                Write(GetRandomBlockAutoSeed(size), filePath);
            else if (cbAlgo.Text.Equals("Block-Oscillating"))
                Write(GetRandomBlockOscillating(size), filePath);
            else if (cbAlgo.Text.Equals("Block-Cycling"))
                Write(GetRandomBlockCycling(size), filePath);
            else if (cbAlgo.Text.Equals("Block-Chaotic"))
                Write(GetRandomBlockChaotic(size), filePath);
            else if (cbAlgo.Text.Equals("Block-Chain"))
                Write(GetRandomBlockChain(size), filePath);
            else if (cbAlgo.Text.Equals("CTR"))
                Write(GetRandomCtr(size), filePath);
            else if (cbAlgo.Text.Equals("CTR-Dual"))
                Write(GetRandomCtrDual(size), filePath);
            else if (cbAlgo.Text.Equals("CTR-Dual-Chaotic"))
                Write(GetRandomCtrDualChaotic(size), filePath);
            else if (cbAlgo.Text.Equals("CSRG"))
                Write(GetRandomCSRG(size), filePath);

            lblStatAes.Text = "Completed in " + TimeSpan.FromMilliseconds(TimerStop()).ToString(@"m\:ss\.ff") + " seconds";
        }

        private void OnSelectedValueChanged(object sender, EventArgs e)
        {
            ComboBox cb = sender as ComboBox;
            if (cb == null) return;
            if (cb.SelectedItem == null) return;

            if (cb.SelectedItem.ToString().Contains("2 * 50000"))
            {
                this.Iterations = 2;
                this.SampleSize = 51200000;
            }
            else if (cb.SelectedItem.ToString().Contains("10 * 10000"))
            {
                this.Iterations = 10;
                this.SampleSize = 10240000;
            }
            else if (cb.SelectedItem.ToString().Contains("100 * 1000"))
            {
                this.Iterations = 100;
                this.SampleSize = 1024000;
            }
            else if (cb.SelectedItem.ToString().Contains("1000 * 100"))
            {
                this.Iterations = 1000;
                this.SampleSize = 102400;
            }
            else
            {
                this.Iterations = 10000;
                this.SampleSize = 10240;
            }

            int sum = this.Iterations * this.SampleSize;

            btnTestAesCtr.Enabled = (Directory.Exists(Path.GetDirectoryName(_dlgPath)));
            btnSaveAlgo.Enabled = btnTestAesCtr.Enabled;

            if (sum > 102400000)
                MessageBox.Show("That is 2 * " + sum.ToString() + "Kib.. This could take a long time to calculate..", "AesCtr", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
        }

        private void OnTestClick(object sender, EventArgs e)
        {
            if (!Directory.Exists(Path.GetDirectoryName(_dlgPath))) return;
            if (this.Iterations < 1 || this.SampleSize < 1) return;

            _Opponent = rdRng.Checked ? ApiTypes.Rng : ApiTypes.Drbg;
            lblStatAes.Text = "....";
            lblStatOpponent.Text = "....";
            _milliSecondsOpponent = 0;
            _milliSecondsAes = 0;
            // set up the controls
            pbStatus.Maximum = Iterations;
            pbStatus.Value = 0;
            lblStatus.Text = "Processing 2 * " + (this.Iterations * this.SampleSize).ToString() + " bytes..";
            lblStatus.Update();
            btnTestAesCtr.Enabled = false;
            btnSaveAlgo.Enabled = false;
            grpOutput.Enabled = false;
            grpModes.Enabled = false;
            grpChoice.Enabled = false;
            grpParamaters.Enabled = false;
            grpSamples.Enabled = false;

            this.Update();

            if (File.Exists(_dlgPath))
                File.Delete(_dlgPath);

            if (chkBitmaps.Checked)
                BitmapGenerator();

            // run the compare
            using (_dataWriter = File.AppendText(_dlgPath))
                CompareApi(Iterations, _dlgPath, false);

            // timed
            lblStatAes.Text = "Aes: " + TimeSpan.FromMilliseconds(_milliSecondsAes).ToString(@"m\:ss\.ff") + " seconds";
            lblStatOpponent.Text = _Opponent.ToString() + ": " + TimeSpan.FromMilliseconds(_milliSecondsOpponent).ToString(@"m\:ss\.ff") + " seconds";
            // enable and notify
            grpOutput.Enabled = true;
            grpModes.Enabled = true;
            grpChoice.Enabled = true;
            grpParamaters.Enabled = true;
            grpSamples.Enabled = true;
            lblStatus.Text = "Completed!";
            pbStatus.Value = Iterations;
            btnOpenFile.Enabled = File.Exists(txtOutput.Text);
            btnSaveAlgo.Enabled = true;
        }
        #endregion

        #region Algorithm Tests
        /*// add a (class) counter in CSRG, this table seed method, write to console when exiting the Generate method, divide last num by 1000:
        CSRG _random = new CSRG();
        public void TransitionsCounter(byte[] TS)
        {
            _expansionTable = ExpandTable(TS);
        }
        private void GetAvgTransitions()
        {
            for (int i = 0; i < 1000; i++)
            {
                _random.BT(CSPRNG.GetSeed48());
                _random.Generate(10240, CSPRNG.GetSeed64());

            }
        }*/
        /// <summary>
        /// run after each nist test to gather the results in a folder
        /// </summary>

        private void CompareSHA()
        {
            byte[] seed1 = CSPRNG.GetSeed16();
            byte[] seed2 = CSPRNG.GetSeed32();
            byte[] seed3 = CSPRNG.GetSeed64();
            byte[] data1 = new byte[32];
            byte[] data2 = new byte[32];

            Console.WriteLine("SHA256 Equality Tests:");

            using (SHA256 shaHash = SHA256Managed.Create())
                data1 = shaHash.ComputeHash(seed1);
            using (SHA256Digest sha = new SHA256Digest())
                data2 = sha.ComputeHash(seed1);

            if (!data1.SequenceEqual(data2))
                Console.WriteLine("SHA Equality Test 1 Failed! Arrays are not equal!");
            else
                Console.WriteLine("SHA Equality Test 1 Passed!");

            using (SHA256 shaHash = SHA256Managed.Create())
                data1 = shaHash.ComputeHash(seed2);
            using (SHA256Digest sha = new SHA256Digest())
                data2 = sha.ComputeHash(seed2);

            if (!data1.SequenceEqual(data2))
                Console.WriteLine("SHA Equality Test 2 Failed! Arrays are not equal!");
            else
                Console.WriteLine("SHA Equality Test 2 Passed!");

            using (SHA256 shaHash = SHA256Managed.Create())
                data1 = shaHash.ComputeHash(seed3);
            using (SHA256Digest sha = new SHA256Digest())
                data2 = sha.ComputeHash(seed3);

            if (!data1.SequenceEqual(data2))
                Console.WriteLine("SHA Equality Test 3 Failed! Arrays are not equal!");
            else
                Console.WriteLine("SHA Equality Test 3 Passed!");
        }

        private void CompareInternalSHA()
        {
            byte[] seed = CSPRNG.GetSeed64();
            byte[] data1 = new byte[32];
            byte[] data2 = new byte[32];

            using (SHA256 shaHash = SHA256Managed.Create())
                data1 = shaHash.ComputeHash(seed);

            // optimized for blocksize hashes only (64 bytes)!
            using (AesCtr ctr = new AesCtr())
                data2 = ctr.ComputeHash64(seed);

            if (!data1.SequenceEqual(data2))
                Console.WriteLine("Internal SHA Equality Test Failed! Arrays are not equal!");
            else
                Console.WriteLine("Internal SHA Equality Test Passed!");
        }

        private void CompareApi(int Iterations, string Path, bool SummaryOnly = false)
        {
            int percentA = 0;
            int percentR = 0;
            double entropyAvgA = 0;
            double entropyAvgR = 0;
            double chiProbA = 0;
            double chiProbR = 0;
            double chiSquareA = 0;
            double chiSquareR = 0;
            double montePiA = 0;
            double montePiR = 0;
            double monteErrA = 0;
            double monteErrR = 0;
            double meanA = 0;
            double meanR = 0;
            double serA = 0;
            double serR = 0;
            int chiProbWon = 0;
            int chiSquareWon = 0;
            int meanWon = 0;
            int monteErrWon = 0;
            int montePiWon = 0;
            int serCorWon = 0;
            ApiTypes api = ApiTypes.Aes;
            EntResult res1;
            EntResult res2;
            Ent ent1 = new Ent();
            Ent ent2 = new Ent();

            for (int i = 0; i < Iterations; i++)
            {
                if (rdCycling.Checked)
                    res1 = ent1.Calculate(GetRandomBlockCycling(SampleSize));
                else if (rdOscillating.Checked)
                    res1 = ent1.Calculate(GetRandomBlockOscillating(SampleSize));
                else if (rdCtrChain.Checked)
                    res1 = ent1.Calculate(GetRandomBlockChain(SampleSize));
                else if (rdChaotic.Checked)
                    res1 = ent1.Calculate(GetRandomBlockChaotic(SampleSize));
                else if (rdAutoSeed.Checked)
                    res1 = ent1.Calculate(GetRandomBlockAutoSeed(SampleSize));
                else if (rdCtr.Checked)
                    res1 = ent1.Calculate(GetRandomCtr(SampleSize));
                else if (rdCtrDual.Checked)
                    res1 = ent1.Calculate(GetRandomCtrDual(SampleSize));
                else if (rdCtrDualCha.Checked)
                    res1 = ent1.Calculate(GetRandomCtrDualChaotic(SampleSize));
                else
                    res1 = ent1.Calculate(GetRandomCSRG(SampleSize));

                if (rdAes800.Checked)
                    res2 = ent2.Calculate(GetAes800Drbg(SampleSize));
                else
                    res2 = ent2.Calculate(GetRNGRandom(SampleSize));

                ApiTypes bestPerc = CompareEnt(res1, res2, TestTypes.Entropy);

                if (bestPerc == ApiTypes.Aes)
                    percentA++;
                else
                    percentR++;

                chiProbA += res1.ChiProbability;
                chiProbR += res2.ChiProbability;
                chiSquareA += res1.ChiSquare;
                chiSquareR += res2.ChiSquare;
                entropyAvgA += res1.Entropy;
                entropyAvgR += res2.Entropy;
                monteErrA += res1.MonteCarloErrorPct;
                monteErrR += res2.MonteCarloErrorPct;
                montePiA += res1.MonteCarloPiCalc;
                montePiR += res2.MonteCarloPiCalc;
                meanA += res1.Mean;
                meanR += res2.Mean;
                serA += res1.SerialCorrelation;
                serR += res2.SerialCorrelation;

                // flag the test winner (A or R)
                if (!SummaryOnly) Append("Entropy Alg:" + bestPerc.ToString() + " AesCtr:" + res1.Entropy.ToString() + " RngApi: " + res2.Entropy.ToString(), Path);

                api = CompareEnt(res1, res2, TestTypes.ChiProbability);
                if (api == ApiTypes.Aes) chiProbWon++;
                if (!SummaryOnly) Append("ChiProbability: " + api.ToString(), Path);
                api = CompareEnt(res1, res2, TestTypes.ChiSquare);
                if (api == ApiTypes.Aes) chiSquareWon++;
                if (!SummaryOnly) Append("ChiSquare: " + api.ToString(), Path);
                api = CompareEnt(res1, res2, TestTypes.Mean);
                if (api == ApiTypes.Aes) meanWon++;
                if (!SummaryOnly) Append("Mean: " + api.ToString(), Path);
                api = CompareEnt(res1, res2, TestTypes.MonteCarloErrorPct);
                if (api == ApiTypes.Aes) monteErrWon++;
                if (!SummaryOnly) Append("MonteCarloErrorPct: " + api.ToString(), Path);
                api = CompareEnt(res1, res2, TestTypes.MonteCarloPiCalc);
                if (api == ApiTypes.Aes) montePiWon++;
                if (!SummaryOnly) Append("MonteCarloPiCalc: " + api.ToString(), Path);
                api = CompareEnt(res1, res2, TestTypes.SerialCorrelation);
                if (api == ApiTypes.Aes) serCorWon++;
                if (!SummaryOnly) Append("SerialCorrelation: " + api.ToString(), Path);
                if (!SummaryOnly) Append(" ", Path);

                if (!SummaryOnly)
                {
                    // Aes scores
                    Append("AesCtr", Path);
                    Append("ChiProbability: " + res1.ChiProbability.ToString(), Path);
                    Append("ChiSquare: " + res1.ChiSquare.ToString(), Path);
                    Append("Mean: " + res1.Mean.ToString(), Path);
                    Append("MonteCarloErrorPct: " + res1.MonteCarloErrorPct.ToString(), Path);
                    Append("MonteCarloPiCalc: " + res1.MonteCarloPiCalc.ToString(), Path);
                    Append("SerialCorrelation: " + res1.SerialCorrelation.ToString(), Path);
                    Append(" ", Path);
                    // rng scores
                    if (!SummaryOnly) Append("RngApi", Path);
                    Append("ChiProbability: " + res2.ChiProbability.ToString(), Path);
                    Append("ChiSquare: " + res2.ChiSquare.ToString(), Path);
                    Append("Mean: " + res2.Mean.ToString(), Path);
                    Append("MonteCarloErrorPct: " + res2.MonteCarloErrorPct.ToString(), Path);
                    Append("MonteCarloPiCalc: " + res2.MonteCarloPiCalc.ToString(), Path);
                    Append("SerialCorrelation: " + res2.SerialCorrelation.ToString(), Path);
                    Append("#######################################################", Path);
                    Append(" ", Path);
                }
                pbStatus.Value = i;
                ent1.Reset();
                ent2.Reset();
            }

            // get the averages
            int sumA = chiProbWon + chiSquareWon + meanWon + monteErrWon + montePiWon + serCorWon;
            int sumR = (Iterations * 6) - sumA;

            Append("Best Overall Entropy: Aes: " + percentA.ToString() + " " + _Opponent.ToString() + ": " + percentR.ToString(), Path);
            Append(" ", Path);
            Append("Entropy Avg: Aes: " + (entropyAvgA / Iterations).ToString() + " " + _Opponent.ToString() + ": " + (entropyAvgR / Iterations).ToString(), Path);
            Append(" ", Path);
            Append("Total Wins of Tests Scored: Aes: " + sumA.ToString() + " " + _Opponent.ToString() + ": " + sumR.ToString(), Path);
            Append(" ", Path);
            Append("ChiProbability Score: Aes: " + chiProbWon.ToString() + " " + _Opponent.ToString() + ": " + (Iterations - chiProbWon).ToString(), Path);
            Append("ChiProbability Avg: Aes: " + (chiProbA / Iterations).ToString() + " " + _Opponent.ToString() + ": " + (chiProbR / Iterations).ToString(), Path);
            Append(" ", Path);
            Append("ChiSquare Score: Aes: " + chiSquareWon.ToString() + " " + _Opponent.ToString() + ": " + (Iterations - chiSquareWon).ToString(), Path);
            Append("ChiSquare Avg: Aes: " + (chiSquareA / Iterations).ToString() + " " + _Opponent.ToString() + ": " + (chiSquareR / Iterations).ToString(), Path);
            Append(" ", Path);
            Append("MonteCarlo Error Score: Aes: " + monteErrWon.ToString() + " " + _Opponent.ToString() + ": " + (Iterations - monteErrWon).ToString(), Path);
            Append("MonteCarlo Error Avg: Aes: " + (monteErrA / Iterations).ToString() + " " + _Opponent.ToString() + ": " + (monteErrR / Iterations).ToString(), Path);
            Append(" ", Path);
            Append("MonteCarlo Pi Score: Aes: " + montePiWon.ToString() + " " + _Opponent.ToString() + ": " + (Iterations - montePiWon).ToString(), Path);
            Append("MonteCarlo Pi Avg: Aes: " + (montePiA / Iterations).ToString() + " " + _Opponent.ToString() + ": " + (montePiR / Iterations).ToString(), Path);
            Append(" ", Path);
            Append("Mean Score: Aes: " + meanWon.ToString() + " " + _Opponent.ToString() + ": " + (Iterations - meanWon).ToString(), Path);
            Append("Mean Avg: Aes: " + (meanA / Iterations).ToString() + " " + _Opponent.ToString() + ": " + (meanR / Iterations).ToString(), Path);
            Append(" ", Path);
            Append("Serial Correlation Score: Aes: " + serCorWon.ToString() + " " + _Opponent.ToString() + ": " + (Iterations - serCorWon).ToString(), Path);
            Append("Serial Correlation Avg: Aes: " + (serA / Iterations).ToString() + " " + _Opponent.ToString() + ": " + (serR / Iterations).ToString(), Path);

            ent1.Dispose();
            ent2.Dispose();
        }

        private void CompareEqual()
        {
            byte[] data1 = new byte[1024000];
            byte[] data2 = new byte[1024000];
            byte[] seed = CSPRNG.GetSeed64();

            Console.WriteLine("Dual Run Equality Tests:");

            // block chain
            using (AesCtr ctr = new AesCtr())
                data1 = ctr.Generate(1024000, seed, EnginesOld.BlockChain);
            using (AesCtr ctr = new AesCtr())
                data2 = ctr.Generate(1024000, seed, EnginesOld.BlockChain);

            if (Equal(data1, data2))
                Console.WriteLine("Block Chaining Equality Test: Passed!");
            else
                Console.WriteLine("Block Chaining Equality Test: Failed!");

            using (AesCtr ctr = new AesCtr())
                data1 = ctr.Generate(1024000, seed, EnginesOld.BlockChaotic);
            using (AesCtr ctr = new AesCtr())
                data2 = ctr.Generate(1024000, seed, EnginesOld.BlockChaotic);

            if (Equal(data1, data2))
                Console.WriteLine("Chaining Chaotic Equality Test: Passed!");
            else
                Console.WriteLine("Chaining Chaotic Equality Test: Failed!");

            using (AesCtr ctr = new AesCtr())
                data1 = ctr.Generate(1024000, seed, EnginesOld.BlockCycling);
            using (AesCtr ctr = new AesCtr())
                data2 = ctr.Generate(1024000, seed, EnginesOld.BlockCycling);

            if (Equal(data1, data2))
                Console.WriteLine("Chaining Cycling Equality Test: Passed!");
            else
                Console.WriteLine("Chaining Cycling Equality Test: Failed!");

            using (AesCtr ctr = new AesCtr())
                data1 = ctr.Generate(1024000, seed, EnginesOld.BlockOscillating);
            using (AesCtr ctr = new AesCtr())
                data2 = ctr.Generate(1024000, seed, EnginesOld.BlockOscillating);

            if (Equal(data1, data2))
                Console.WriteLine("Chaining Oscillating Equality Test: Passed!");
            else
                Console.WriteLine("Chaining Oscillating Equality Test: Failed!");

            // ctr
            using (AesCtr ctr = new AesCtr())
                data1 = ctr.Generate(1024000, seed, EnginesOld.CTR);
            using (AesCtr ctr = new AesCtr())
                data2 = ctr.Generate(1024000, seed, EnginesOld.CTR);

            if (Equal(data1, data2))
                Console.WriteLine("CTR Equality Test: Passed!");
            else
                Console.WriteLine("CTR Equality Test: Failed!");

            using (AesCtr ctr = new AesCtr())
                data1 = ctr.Generate(1024000, seed, EnginesOld.CTRDual);
            using (AesCtr ctr = new AesCtr())
                data2 = ctr.Generate(1024000, seed, EnginesOld.CTRDual);

            if (Equal(data1, data2))
                Console.WriteLine("Dual CTR Equality Test: Passed!");
            else
                Console.WriteLine("Dual CTR Equality Test: Failed!");

            using (AesCtr ctr = new AesCtr())
                data1 = ctr.Generate(1024000, seed, EnginesOld.CTRDualChaotic);
            using (AesCtr ctr = new AesCtr())
                data2 = ctr.Generate(1024000, seed, EnginesOld.CTRDualChaotic);

            if (Equal(data1, data2))
                Console.WriteLine("Dual CTR Chaotic Equality Test: Passed!");
            else
                Console.WriteLine("Dual CTR Chaotic Equality Test: Failed!");

            // CSRG
            byte[] table = CSPRNG.GetSeed48();

            using (CSRG csrg = new CSRG(table))
                data1 = csrg.Generate(10240, seed);
            using (CSRG csrg = new CSRG(table))
                data2 = csrg.Generate(10240, seed);

            if (Equal(data1, data2))
                Console.WriteLine("CSRG Equality Test: Passed!");
            else
                Console.WriteLine("CSRG Equality Test: Failed!");
        }

        private void CompareSpeed(int Size)
        {
            byte[] data = new byte[Size];
            byte[] seed512 = CSPRNG.GetSeed64();
            byte[] seed384 = CSPRNG.GetSeed48();
            string state = " created " + Size.ToString() +  " bytes in ";

            Console.WriteLine("Algorithm Speed Tests:");

            // start
            TimerStart();
            // AesCtr
            using (AesCtr random = new AesCtr())
                data = random.Generate(Size, seed512, EnginesOld.BlockChain);
            // get results and reset
            Console.WriteLine("AesCtr" + state + TimeSpan.FromMilliseconds(TimerStop()).ToString(@"m\:ss\.ff") + " seconds");

            data = new byte[Size];
            TimerStart();
            // RNGCryptoServiceProvider
            _rngRandom.GetBytes(data);
            Console.WriteLine("RngCrypto" + state + TimeSpan.FromMilliseconds(TimerStop()).ToString(@"m\:ss\.ff") + " seconds");

            data = new byte[Size];
            // Aes800Drbg
            TimerStart();
            Aes800Drbg ctr = new Aes800Drbg(CSPRNG.GetSeed48());
            ctr.Generate(data);
            Console.WriteLine("800Drbg" + state + TimeSpan.FromMilliseconds(TimerStop()).ToString(@"m\:ss\.ff") + " seconds");
        }

        private ApiTypes CompareEnt(EntResult Res1, EntResult Res2, TestTypes TestType = TestTypes.Entropy)
        {
            const double MEAN = 127.5;
            const double PI = 3.1415926535;

            ApiTypes winner = ApiTypes.Aes;

            switch (TestType)
            {
                case TestTypes.Entropy:
                    {
                        if (Res2.Entropy > Res1.Entropy)
                            winner = _Opponent;
                    }
                    break;
                case TestTypes.ChiProbability:
                    {
                        if (Math.Abs(Res1.ChiProbability - 0.5) > Math.Abs(Res2.ChiProbability - 0.5))
                            winner = _Opponent;
                    }
                    break;
                case TestTypes.ChiSquare:
                    {
                        if (Math.Abs(Res1.ChiSquare - 256.0) > Math.Abs(Res2.ChiSquare - 256.0))
                            winner = _Opponent;
                    }
                    break;
                case TestTypes.Mean:
                    {
                        if (Math.Abs(MEAN - Res1.Mean) > Math.Abs(MEAN - Res2.Mean))
                            winner = _Opponent;
                    }
                    break;
                case TestTypes.MonteCarloErrorPct:
                    {
                        if (Math.Abs(Res1.MonteCarloErrorPct) > Math.Abs(Res2.MonteCarloErrorPct))
                            winner = _Opponent;
                    }
                    break;
                case TestTypes.MonteCarloPiCalc:
                    {
                        if (Math.Abs(PI - Res1.MonteCarloPiCalc) > Math.Abs(PI - Res2.MonteCarloPiCalc))
                            winner = _Opponent;
                    }
                    break;
                case TestTypes.SerialCorrelation:
                    {
                        if (Math.Abs(Res1.SerialCorrelation) > Math.Abs(Res2.SerialCorrelation))
                            winner = _Opponent;
                    }
                    break;

            }
            return winner;
        }

        private void TestOutputs()
        {
            try
            {
                Console.WriteLine("Testing CPRNG..");
                Console.WriteLine("Double: " + CSPRNG.NextDouble().ToString());
                Console.WriteLine("Float: " + CSPRNG.NextFloat().ToString());
                Console.WriteLine("Int16: " + CSPRNG.NextInt16().ToString());
                Console.WriteLine("UInt16: " + CSPRNG.NextUInt16().ToString());
                Console.WriteLine("Int32: " + CSPRNG.NextInt32().ToString());
                Console.WriteLine("UInt32: " + CSPRNG.NextUInt32().ToString());
                Console.WriteLine("Int64: " + CSPRNG.NextInt64().ToString());
                Console.WriteLine("UInt64: " + CSPRNG.NextUInt64().ToString());

                Console.WriteLine("Testing AesCtr..");
                using (AesCtr random = new AesCtr())
                {
                    Int16[] num16 = new Int16[8];
                    random.GetInt16s(num16);
                    Console.WriteLine("Int16 array: " + string.Join(",", num16));

                    Int32[] num32 = new Int32[8];
                    random.GetInt32s(num32);
                    Console.WriteLine("Int32 array: " + string.Join(",", num32));

                    Int64[] num64 = new Int64[8];
                    random.GetInt64s(num64);
                    Console.WriteLine("Int64 array: " + string.Join(",", num64));

                    byte[] btA = new byte[8];
                    random.GetBytes(btA);
                    Console.WriteLine("byte array: " + string.Join(",", btA));

                    char[] chA = new char[8];
                    random.GetChars(chA);
                    Console.WriteLine("char array: " + string.Join(",", chA));
                }
            }
            catch (Exception ex)
            {
                throw new Exception("TestOutputs() has failed!");
            }
        }
        #endregion

        #region Competing Algorithms
        /// <summary>
        /// Uses the native RNGCryptoServiceProvider algorithm
        /// </summary>
        /// <param name="Size">Size of return in bytes</param>
        /// <returns>Random byte array [byte]]</returns>
        private byte[] GetRNGRandom(int Size)
        {
            byte[] data = new byte[Size];
            TimerStart();
            _rngRandom.GetBytes(data);
            _milliSecondsOpponent += TimerStop();
            return data;
        }

        /// <summary>
        /// Get random using the Aes800Drbg algorithm 
        /// </summary>
        private byte[] GetAes800Drbg(int Size)
        {
            byte[] data = new byte[Size];
            TimerStart();
            Aes800Drbg ctr = new Aes800Drbg(CSPRNG.GetSeed48());
            ctr.Generate(data);
            _milliSecondsOpponent += TimerStop();
            return data;
        }
        #endregion

        #region Block-Chain Algorithms
        /// <summary>
        /// Get random using AesCtr in auto seeding mode
        /// </summary>
        private byte[] GetRandomBlockAutoSeed(int Size)
        {
            byte[] data = new byte[Size];
            TimerStart();
            using (AesCtr random = new AesCtr())
                data = random.Generate(Size);
            _milliSecondsAes += TimerStop();
            return data;
        }

        /// <summary>
        /// Generate a block of random bytes, using CBC block chaining and random state resets.
        /// </summary>
        private byte[] GetRandomBlockChaotic(int Size)
        {
            byte[] data = new byte[Size];
            TimerStart();
            using (AesCtr random = new AesCtr())
                data = random.Generate(Size, CSPRNG.GetSeed64(), EnginesOld.BlockChaotic);
            _milliSecondsAes += TimerStop();
            return data;
        }

        /// <summary>
        /// Generate a block of random bytes, using CBC block chaining.
        /// </summary>
        private byte[] GetRandomBlockChain(int Size)
        {
            byte[] data = new byte[Size];
            TimerStart();
            using (AesCtr random = new AesCtr())
                data = random.Generate(Size, CSPRNG.GetSeed64(), EnginesOld.BlockChain);
            _milliSecondsAes += TimerStop();
            return data;
        }

        /// <summary>
        /// Generate a block of random bytes, using CBC block chaining and a cycling key scheme.
        /// </summary>
        private byte[] GetRandomBlockCycling(int Size)
        {
            byte[] data = new byte[Size];
            TimerStart();
            using (AesCtr random = new AesCtr())
                data = random.Generate(Size, CSPRNG.GetSeed64(), EnginesOld.BlockCycling);
            _milliSecondsAes += TimerStop();
            return data;
        }

        /// <summary>
        /// Generate a block of random bytes, using CBC block chaining and an oscillating key scheme.
        /// </summary>
        private byte[] GetRandomBlockOscillating(int Size)
        {
            byte[] data = new byte[Size];
            TimerStart();
            using (AesCtr random = new AesCtr())
                data = random.Generate(Size, CSPRNG.GetSeed64(), EnginesOld.BlockOscillating);
            _milliSecondsAes += TimerStop();
            return data;
        }
        #endregion

        #region CTR Algorithms
        /// <summary>
        /// Generate a block of random bytes, using Aes Counter mode.
        /// </summary>
        private byte[] GetRandomCtr(int Size)
        {
            byte[] data = new byte[Size];
            TimerStart();
            using (AesCtr random = new AesCtr())
                data = random.Generate(Size, CSPRNG.GetSeed64(), EnginesOld.CTR);
            _milliSecondsAes += TimerStop();
            return data;
        }

        /// <summary>
        /// Generate a block of random bytes, XORs Dual independant Aes Counter transforms.
        /// </summary>
        private byte[] GetRandomCtrDual(int Size)
        {
            byte[] data = new byte[Size];
            TimerStart();
            using (AesCtr random = new AesCtr())
                data = random.Generate(Size, CSPRNG.GetSeed64(), EnginesOld.CTRDual);
            _milliSecondsAes += TimerStop();
            return data;
        }

        /// <summary>
        /// Generate a block of random bytes, XORs Dual independant Aes Counter transforms using random state resets.
        /// </summary>
        private byte[] GetRandomCtrDualChaotic(int Size)
        {
            byte[] data = new byte[Size];
            TimerStart();
            using (AesCtr random = new AesCtr())
                data = random.Generate(Size, CSPRNG.GetSeed64(), EnginesOld.CTRDualChaotic);
            _milliSecondsAes += TimerStop();
            return data;
        }
        #endregion

        #region CSRG
        /// <summary>
        /// Use the CSRG to generate a random block.
        /// </summary>
        private byte[] GetRandomCSRG(int Size)
        {
            byte[] data = new byte[Size];
            byte[] tableKey = CSPRNG.GetSeed48();

            TimerStart();
            using (CSRG random = new CSRG(tableKey))
                data = random.Generate(Size, CSPRNG.GetSeed64());
            _milliSecondsAes += TimerStop();
            return data;
        }
        #endregion

        #region Bitmap Generation
        private void BitmapGenerator()
        {
            byte[] data = new byte[0];
            string path = string.Empty;
            string dir = Path.GetDirectoryName(_dlgPath);

            const int SAMPLE = 102400;

            if (!Directory.Exists(dir)) return;

            // RNG and aes800
            if (rdAes800.Checked)
            {
                path = Path.Combine(dir, "aes800.bmp");
                data = GetAes800Drbg(SAMPLE);
            }
            else if (rdRng.Checked)
            {
                path = Path.Combine(dir, "rngcrypto.bmp");
                data = GetRNGRandom(SAMPLE);
            }

            // 1st image (them)
            if (File.Exists(path))
                File.Delete(path);
            if (data.Length > 0)
                GetRandomMap(data).Save(path);

            // AesCtr Modes
            if (rdCycling.Checked)
            {
                path = Path.Combine(dir, "aes-cycling.bmp");
                data = GetRandomBlockCycling(SAMPLE);
            }
            else if (rdOscillating.Checked)
            {
                path = Path.Combine(dir, "aes-oscillating.bmp");
                data = GetRandomBlockOscillating(SAMPLE);
            }
            else if (rdCtrChain.Checked)
            {
                path = Path.Combine(dir, "aes-ctrchain.bmp");
                data = GetRandomBlockChain(SAMPLE);
            }
            else if (rdChaotic.Checked)
            {
                path = Path.Combine(dir, "aes-chaotic.bmp");
                data = GetRandomBlockChaotic(SAMPLE);
            }
            else
            {
                path = Path.Combine(dir, "aes-autoseed.bmp");
                data = GetRandomBlockAutoSeed(SAMPLE);
            }

            // 2nd image (us)
            if (File.Exists(path))
                File.Delete(path);
            if (data.Length > 0)
                GetRandomMap(data).Save(path);
        }

        private Bitmap GetRandomMap(byte[] Data)
        {
            int size = (int)Math.Sqrt(Data.Length / 4) + 1;

            // create
            Bitmap bmp = new Bitmap(size, size, PixelFormat.Format32bppPArgb);

            BitmapData bmpData = bmp.LockBits(
                                 new Rectangle(0, 0, bmp.Width, bmp.Height),
                                 ImageLockMode.WriteOnly, bmp.PixelFormat);

            // copy to Scan0
            Marshal.Copy(Data, 0, bmpData.Scan0, Data.Length);
            bmp.UnlockBits(bmpData);

            return bmp;
        }
        #endregion

        #region Helpers
        private void Append(string Data, string Path)
        {
            _dataWriter.WriteLine(Data);
        }

        private bool Equal(byte[] Data1, byte[] Data2)
        {
            return Data1.SequenceEqual(Data2);
        }

        private void TimerStart()
        {
            // start
            _runTimer.Reset();
            _runTimer.Start();
        }

        private double TimerStop()
        {
            // get results and reset
            _runTimer.Stop();
            return _runTimer.Elapsed.Milliseconds;
        }

        private void Write(byte[] Data, string Path)
        {
            using (BinaryWriter writer = new BinaryWriter(File.Open(Path, FileMode.Create)))
                writer.Write(Data);
        }
        #endregion

        #region Tools
        private void ByteToHex(byte[] Data)
        {
            string byteStr = string.Empty;

            for (int i = 1; i < Data.Length + 1; i++)
            {
                byteStr += String.Format("0x{0:X}", Data[i - 1]) + ", ";
                if (i % 8 == 0 && i > 0)
                {
                    Console.WriteLine(byteStr);
                    byteStr = string.Empty;
                }
            }
        }

        private string GetUniquePath(string DirectoryPath, string FileName, string FileExtension)
        {
            string filePath = Path.Combine(DirectoryPath, FileName + FileExtension);
            if (!File.Exists(filePath)) return filePath;

            for (int j = 0; j < 100; j++)
            {
                // test unique names
                if (File.Exists(filePath))
                    filePath = Path.Combine(DirectoryPath, FileName + j.ToString() + FileExtension);
                else
                    break;
            }
            return filePath;
        }
        #endregion
    }
}



