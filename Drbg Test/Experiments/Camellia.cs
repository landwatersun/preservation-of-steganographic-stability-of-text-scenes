﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Drbg_Test
{
    public class Camellia
    {
        private const int EXPANDED_KEYSIZE = 96;
        private const int BLOCK_SIZE = 16;
        private UInt32[] KW = new UInt32[4 * 2];
        private UInt32[] KE = new UInt32[6 * 2];
        private UInt32[] STATE = new UInt32[4];

        #region Constructor
        public Camellia()
        {
        }

        public byte[] EncryptTest(byte[] Input, byte[] Key)
        {
            UInt32[] exKey = ExpandKey(Key);
            byte[] block = new byte[16];
            CamelliaTransform(Input, 0, block, 0, exKey);

            return block;
        }
        #endregion

        #region Properties
        public int BlockSize
        {
            get { return BLOCK_SIZE; }
        }
        #endregion

        #region Transform
        public Int32 CamelliaTransform(byte[] Input, Int32 InputOffset, byte[] Output, Int32 OutputOffset, UInt32[] Key)
        {
            for (int i = 0; i < 4; i++)
            {
                STATE[i] = BytesToUint(Input, InputOffset + (i * 4));
                STATE[i] ^= KW[i];
            }

            CamelliaF2(STATE, Key, 0);
            CamelliaF2(STATE, Key, 4);
            CamelliaF2(STATE, Key, 8);
            CamelliaFLs(STATE, KE, 0);
            CamelliaF2(STATE, Key, 12);
            CamelliaF2(STATE, Key, 16);
            CamelliaF2(STATE, Key, 20);
            CamelliaFLs(STATE, KE, 4);
            CamelliaF2(STATE, Key, 24);
            CamelliaF2(STATE, Key, 28);
            CamelliaF2(STATE, Key, 32);
            CamelliaFLs(STATE, KE, 8);
            CamelliaF2(STATE, Key, 36);
            CamelliaF2(STATE, Key, 40);
            CamelliaF2(STATE, Key, 44);

            STATE[2] ^= KW[4];
            STATE[3] ^= KW[5];
            STATE[0] ^= KW[6];
            STATE[1] ^= KW[7];

            UintToBytes(STATE[2], Output, OutputOffset);
            UintToBytes(STATE[3], Output, OutputOffset + 4);
            UintToBytes(STATE[0], Output, OutputOffset + 8);
            UintToBytes(STATE[1], Output, OutputOffset + 12);

            return BLOCK_SIZE;
        }
        #endregion

        #region Helpers
        private UInt32 RotateRight(UInt32 X, Int32 Offset)
        {
            return ((X >> Offset) + (X << (32 - Offset)));
        }

        private UInt32 RotateLeft(UInt32 X, Int32 Offset)
        {
            return (X << Offset) + (X >> (32 - Offset));
        }

        private void RolDq(Int32 Rot, UInt32[] KIn, Int32 InOffset, UInt32[] KOut, Int32 OutOffset)
        {
            KOut[0 + OutOffset] = (KIn[0 + InOffset] << Rot) | (KIn[1 + InOffset] >> (32 - Rot));
            KOut[1 + OutOffset] = (KIn[1 + InOffset] << Rot) | (KIn[2 + InOffset] >> (32 - Rot));
            KOut[2 + OutOffset] = (KIn[2 + InOffset] << Rot) | (KIn[3 + InOffset] >> (32 - Rot));
            KOut[3 + OutOffset] = (KIn[3 + InOffset] << Rot) | (KIn[0 + InOffset] >> (32 - Rot));
            KIn[0 + InOffset] = KOut[0 + OutOffset];
            KIn[1 + InOffset] = KOut[1 + OutOffset];
            KIn[2 + InOffset] = KOut[2 + OutOffset];
            KIn[3 + InOffset] = KOut[3 + OutOffset];
        }

        private void DecRolDq(Int32 Rot, UInt32[] KIn, Int32 InOffset, UInt32[] KOut, Int32 OutOffset)
        {
            KOut[2 + OutOffset] = (KIn[0 + InOffset] << Rot) | (KIn[1 + InOffset] >> (32 - Rot));
            KOut[3 + OutOffset] = (KIn[1 + InOffset] << Rot) | (KIn[2 + InOffset] >> (32 - Rot));
            KOut[0 + OutOffset] = (KIn[2 + InOffset] << Rot) | (KIn[3 + InOffset] >> (32 - Rot));
            KOut[1 + OutOffset] = (KIn[3 + InOffset] << Rot) | (KIn[0 + InOffset] >> (32 - Rot));
            KIn[0 + InOffset] = KOut[2 + OutOffset];
            KIn[1 + InOffset] = KOut[3 + OutOffset];
            KIn[2 + InOffset] = KOut[0 + OutOffset];
            KIn[3 + InOffset] = KOut[1 + OutOffset];
        }

        private void RolDqo32(Int32 Rot, UInt32[] KIn, Int32 InOffset, UInt32[] KOut, Int32 OutOffset)
        {
            KOut[0 + OutOffset] = (KIn[1 + InOffset] << (Rot - 32)) | (KIn[2 + InOffset] >> (64 - Rot));
            KOut[1 + OutOffset] = (KIn[2 + InOffset] << (Rot - 32)) | (KIn[3 + InOffset] >> (64 - Rot));
            KOut[2 + OutOffset] = (KIn[3 + InOffset] << (Rot - 32)) | (KIn[0 + InOffset] >> (64 - Rot));
            KOut[3 + OutOffset] = (KIn[0 + InOffset] << (Rot - 32)) | (KIn[1 + InOffset] >> (64 - Rot));
            KIn[0 + InOffset] = KOut[0 + OutOffset];
            KIn[1 + InOffset] = KOut[1 + OutOffset];
            KIn[2 + InOffset] = KOut[2 + OutOffset];
            KIn[3 + InOffset] = KOut[3 + OutOffset];
        }

        private void DecRolDqo32(Int32 Rot, UInt32[] KIn, Int32 InOffset, UInt32[] KOut, Int32 OutOffset)
        {
            KOut[2 + OutOffset] = (KIn[1 + InOffset] << (Rot - 32)) | (KIn[2 + InOffset] >> (64 - Rot));
            KOut[3 + OutOffset] = (KIn[2 + InOffset] << (Rot - 32)) | (KIn[3 + InOffset] >> (64 - Rot));
            KOut[0 + OutOffset] = (KIn[3 + InOffset] << (Rot - 32)) | (KIn[0 + InOffset] >> (64 - Rot));
            KOut[1 + OutOffset] = (KIn[0 + InOffset] << (Rot - 32)) | (KIn[1 + InOffset] >> (64 - Rot));
            KIn[0 + InOffset] = KOut[2 + OutOffset];
            KIn[1 + InOffset] = KOut[3 + OutOffset];
            KIn[2 + InOffset] = KOut[0 + OutOffset];
            KIn[3 + InOffset] = KOut[1 + OutOffset];
        }

        private UInt32 BytesToUint(byte[] Data, Int32 Offset)
        {
            UInt32 word = 0;

            for (int i = 0; i < 4; i++)
                word = (word << 8) + (UInt32)Data[i + Offset];

            return word;
        }

        private void UintToBytes(UInt32 Input, byte[] Output, Int32 Offset)
        {
            for (int i = 0; i < 4; i++)
            {
                Output[(3 - i) + Offset] = (byte)Input;
                Input >>= 8;
            }
        }

        private byte LRot8(byte Vector, Int32 Rot)
        {
            return (byte)(((UInt32)Vector << Rot) | ((UInt32)Vector >> (8 - Rot)));
        }

        private UInt32 SBox2(Int32 X)
        {
            return (UInt32)LRot8(CSB1[X], 1);
        }

        private UInt32 SBox3(Int32 X)
        {
            return (UInt32)LRot8(CSB1[X], 7);
        }

        private UInt32 SBox4(Int32 X)
        {
            return (UInt32)CSB1[LRot8((byte)X, 1)];
        }

        private void CamelliaF2(UInt32[] Sample, UInt32[] Key, Int32 KeyOffset)
        {
            UInt32 t1, t2, u, v;

            t1 = Sample[0] ^ Key[0 + KeyOffset];
            u = SBox4((byte)t1);
            u |= (SBox3((byte)(t1 >> 8)) << 8);
            u |= (SBox2((byte)(t1 >> 16)) << 16);
            u |= ((UInt32)(CSB1[(byte)(t1 >> 24)]) << 24);

            t2 = Sample[1] ^ Key[1 + KeyOffset];
            v = (UInt32)CSB1[(byte)t2];
            v |= (SBox4((byte)(t2 >> 8)) << 8);
            v |= (SBox3((byte)(t2 >> 16)) << 16);
            v |= (SBox2((byte)(t2 >> 24)) << 24);

            v = RotateLeft(v, 8);
            u ^= v;
            v = RotateLeft(v, 8) ^ u;
            u = RotateRight(u, 8) ^ v;
            Sample[2] ^= RotateLeft(v, 16) ^ u;
            Sample[3] ^= RotateLeft(u, 8);

            t1 = Sample[2] ^ Key[2 + KeyOffset];
            u = SBox4((byte)t1);
            u |= SBox3((byte)(t1 >> 8)) << 8;
            u |= SBox2((byte)(t1 >> 16)) << 16;
            u |= ((UInt32)CSB1[(byte)(t1 >> 24)]) << 24;

            t2 = Sample[3] ^ Key[3 + KeyOffset];
            v = (UInt32)CSB1[(byte)t2];
            v |= SBox4((byte)(t2 >> 8)) << 8;
            v |= SBox3((byte)(t2 >> 16)) << 16;
            v |= SBox2((byte)(t2 >> 24)) << 24;

            v = RotateLeft(v, 8);
            u ^= v;
            v = RotateLeft(v, 8) ^ u;
            u = RotateRight(u, 8) ^ v;
            Sample[0] ^= RotateLeft(v, 16) ^ u;
            Sample[1] ^= RotateLeft(u, 8);
        }

        private void CamelliaFLs(UInt32[] Sample, UInt32[] FKey, Int32 KeyOffset)
        {
            Sample[1] ^= RotateLeft(Sample[0] & FKey[0 + KeyOffset], 1);
            Sample[0] ^= FKey[1 + KeyOffset] | Sample[1];
            Sample[2] ^= FKey[3 + KeyOffset] | Sample[3];
            Sample[3] ^= RotateLeft(FKey[2 + KeyOffset] & Sample[2], 1);
        }

        private UInt32[] ExpandKey(byte[] Key)
        {
            UInt32[] subKey = new UInt32[EXPANDED_KEYSIZE];
            UInt32[] k = new UInt32[8];
            UInt32[] ka = new UInt32[4];
            UInt32[] kb = new UInt32[4];
            UInt32[] t = new UInt32[4];

            k[0] = BytesToUint(Key, 0);
            k[1] = BytesToUint(Key, 4);
            k[2] = BytesToUint(Key, 8);
            k[3] = BytesToUint(Key, 12);
            k[4] = BytesToUint(Key, 16);
            k[5] = BytesToUint(Key, 20);
            k[6] = BytesToUint(Key, 24);
            k[7] = BytesToUint(Key, 28);

            for (int i = 0; i < 4; i++)
                ka[i] = k[i] ^ k[i + 4];

            /* compute KA */
            CamelliaF2(ka, SIGMA, 0);
            for (int i = 0; i < 4; i++)
                ka[i] ^= k[i];

            CamelliaF2(ka, SIGMA, 4);

            // compute KB 
            for (int i = 0; i < 4; i++)
                kb[i] = ka[i] ^ k[i + 4];

            CamelliaF2(kb, SIGMA, 8);

            // KL dependant keys 
            KW[0] = k[0];
            KW[1] = k[1];
            KW[2] = k[2];
            KW[3] = k[3];
            RolDqo32(45, k, 0, subKey, 16);
            RolDq(15, k, 0, KE, 4);
            RolDq(17, k, 0, subKey, 32);
            RolDqo32(34, k, 0, subKey, 44);
            // KR dependant keys 
            RolDq(15, k, 4, subKey, 4);
            RolDq(15, k, 4, KE, 0);
            RolDq(30, k, 4, subKey, 24);
            RolDqo32(34, k, 4, subKey, 36);
            // KA dependant keys 
            RolDq(15, ka, 0, subKey, 8);
            RolDq(30, ka, 0, subKey, 20);
            // 32bit rotation 
            KE[8] = ka[1];
            KE[9] = ka[2];
            KE[10] = ka[3];
            KE[11] = ka[0];
            RolDqo32(49, ka, 0, subKey, 40);
            // KB dependant keys
            subKey[0] = kb[0];
            subKey[1] = kb[1];
            subKey[2] = kb[2];
            subKey[3] = kb[3];
            RolDq(30, kb, 0, subKey, 12);
            RolDq(30, kb, 0, subKey, 28);
            RolDqo32(51, kb, 0, KW, 4);

            return subKey;
        }
        #endregion

        #region Tables
        private readonly UInt32[] SIGMA = {
			0xa09e667f, 0x3bcc908b, 0xb67ae858, 0x4caa73b2, 0xc6ef372f, 0xe94f82be,
			0x54ff53a5, 0xf1d36f1c, 0x10e527fa, 0xde682d1d, 0xb05688c2, 0xb3e6c1fd
		};

        private byte[] CSB1 = new byte[256] {
            0x70, 0x82, 0x2C, 0xEC, 0xB3, 0x27, 0xC0, 0xE5, 
            0xE4, 0x85, 0x57, 0x35, 0xEA, 0xC, 0xAE, 0x41, 
            0x23, 0xEF, 0x6B, 0x93, 0x45, 0x19, 0xA5, 0x21, 
            0xED, 0xE, 0x4F, 0x4E, 0x1D, 0x65, 0x92, 0xBD, 
            0x86, 0xB8, 0xAF, 0x8F, 0x7C, 0xEB, 0x1F, 0xCE, 
            0x3E, 0x30, 0xDC, 0x5F, 0x5E, 0xC5, 0xB, 0x1A, 
            0xA6, 0xE1, 0x39, 0xCA, 0xD5, 0x47, 0x5D, 0x3D, 
            0xD9, 0x1, 0x5A, 0xD6, 0x51, 0x56, 0x6C, 0x4D, 
            0x8B, 0xD, 0x9A, 0x66, 0xFB, 0xCC, 0xB0, 0x2D, 
            0x74, 0x12, 0x2B, 0x20, 0xF0, 0xB1, 0x84, 0x99, 
            0xDF, 0x4C, 0xCB, 0xC2, 0x34, 0x7E, 0x76, 0x5, 
            0x6D, 0xB7, 0xA9, 0x31, 0xD1, 0x17, 0x4, 0xD7, 
            0x14, 0x58, 0x3A, 0x61, 0xDE, 0x1B, 0x11, 0x1C, 
            0x32, 0xF, 0x9C, 0x16, 0x53, 0x18, 0xF2, 0x22, 
            0xFE, 0x44, 0xCF, 0xB2, 0xC3, 0xB5, 0x7A, 0x91, 
            0x24, 0x8, 0xE8, 0xA8, 0x60, 0xFC, 0x69, 0x50, 
            0xAA, 0xD0, 0xA0, 0x7D, 0xA1, 0x89, 0x62, 0x97, 
            0x54, 0x5B, 0x1E, 0x95, 0xE0, 0xFF, 0x64, 0xD2, 
            0x10, 0xC4, 0x0, 0x48, 0xA3, 0xF7, 0x75, 0xDB, 
            0x8A, 0x3, 0xE6, 0xDA, 0x9, 0x3F, 0xDD, 0x94, 
            0x87, 0x5C, 0x83, 0x2, 0xCD, 0x4A, 0x90, 0x33, 
            0x73, 0x67, 0xF6, 0xF3, 0x9D, 0x7F, 0xBF, 0xE2, 
            0x52, 0x9B, 0xD8, 0x26, 0xC8, 0x37, 0xC6, 0x3B, 
            0x81, 0x96, 0x6F, 0x4B, 0x13, 0xBE, 0x63, 0x2E, 
            0xE9, 0x79, 0xA7, 0x8C, 0x9F, 0x6E, 0xBC, 0x8E, 
            0x29, 0xF5, 0xF9, 0xB6, 0x2F, 0xFD, 0xB4, 0x59, 
            0x78, 0x98, 0x6, 0x6A, 0xE7, 0x46, 0x71, 0xBA, 
            0xD4, 0x25, 0xAB, 0x42, 0x88, 0xA2, 0x8D, 0xFA, 
            0x72, 0x7, 0xB9, 0x55, 0xF8, 0xEE, 0xAC, 0xA, 
            0x36, 0x49, 0x2A, 0x68, 0x3C, 0x38, 0xF1, 0xA4, 
            0x40, 0x28, 0xD3, 0x7B, 0xBB, 0xC9, 0x43, 0xC1, 
            0x15, 0xE3, 0xAD, 0xF4, 0x77, 0xC7, 0x80, 0x9E
        };
        #endregion
    }
}
