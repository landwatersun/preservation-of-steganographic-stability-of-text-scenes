﻿namespace Drbg_Test
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnTestAesCtr = new System.Windows.Forms.Button();
            this.btnDialog = new System.Windows.Forms.Button();
            this.txtOutput = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.pbStatus = new System.Windows.Forms.ProgressBar();
            this.grpOutput = new System.Windows.Forms.GroupBox();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.lblStatus = new System.Windows.Forms.Label();
            this.grpChoice = new System.Windows.Forms.GroupBox();
            this.rdAes800 = new System.Windows.Forms.RadioButton();
            this.rdRng = new System.Windows.Forms.RadioButton();
            this.grpModes = new System.Windows.Forms.GroupBox();
            this.rdCSRG = new System.Windows.Forms.RadioButton();
            this.rdCtrDual = new System.Windows.Forms.RadioButton();
            this.rdCtrDualCha = new System.Windows.Forms.RadioButton();
            this.rdCtr = new System.Windows.Forms.RadioButton();
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.rdChaotic = new System.Windows.Forms.RadioButton();
            this.rdCtrChain = new System.Windows.Forms.RadioButton();
            this.rdOscillating = new System.Windows.Forms.RadioButton();
            this.rdCycling = new System.Windows.Forms.RadioButton();
            this.rdAutoSeed = new System.Windows.Forms.RadioButton();
            this.grpParamaters = new System.Windows.Forms.GroupBox();
            this.btnOpenFile = new System.Windows.Forms.Button();
            this.chkBitmaps = new System.Windows.Forms.CheckBox();
            this.chkSummary = new System.Windows.Forms.CheckBox();
            this.cbTestIterations = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblStatAes = new System.Windows.Forms.Label();
            this.lblStatOpponent = new System.Windows.Forms.Label();
            this.grpSamples = new System.Windows.Forms.GroupBox();
            this.lblMib = new System.Windows.Forms.Label();
            this.lblAlgoSize = new System.Windows.Forms.Label();
            this.cbAlgoSize = new System.Windows.Forms.ComboBox();
            this.lblAlgo = new System.Windows.Forms.Label();
            this.btnSaveAlgo = new System.Windows.Forms.Button();
            this.cbAlgo = new System.Windows.Forms.ComboBox();
            this.grpOutput.SuspendLayout();
            this.grpChoice.SuspendLayout();
            this.grpModes.SuspendLayout();
            this.grpParamaters.SuspendLayout();
            this.grpSamples.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnTestAesCtr
            // 
            this.btnTestAesCtr.Location = new System.Drawing.Point(417, 445);
            this.btnTestAesCtr.Name = "btnTestAesCtr";
            this.btnTestAesCtr.Size = new System.Drawing.Size(75, 30);
            this.btnTestAesCtr.TabIndex = 0;
            this.btnTestAesCtr.Text = "Run Tests";
            this.btnTestAesCtr.UseVisualStyleBackColor = true;
            this.btnTestAesCtr.Click += new System.EventHandler(this.OnTestClick);
            // 
            // btnDialog
            // 
            this.btnDialog.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDialog.Location = new System.Drawing.Point(450, 19);
            this.btnDialog.Name = "btnDialog";
            this.btnDialog.Size = new System.Drawing.Size(30, 30);
            this.btnDialog.TabIndex = 1;
            this.btnDialog.Text = "...";
            this.btnDialog.UseVisualStyleBackColor = true;
            this.btnDialog.Click += new System.EventHandler(this.OnDialogClick);
            // 
            // txtOutput
            // 
            this.txtOutput.Location = new System.Drawing.Point(6, 24);
            this.txtOutput.Name = "txtOutput";
            this.txtOutput.Size = new System.Drawing.Size(438, 20);
            this.txtOutput.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(8, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(375, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Run an ENT comparison between AesCtr and common Algorithms";
            // 
            // pbStatus
            // 
            this.pbStatus.Location = new System.Drawing.Point(0, 490);
            this.pbStatus.Name = "pbStatus";
            this.pbStatus.Size = new System.Drawing.Size(206, 16);
            this.pbStatus.TabIndex = 4;
            // 
            // grpOutput
            // 
            this.grpOutput.Controls.Add(this.btnDialog);
            this.grpOutput.Controls.Add(this.txtOutput);
            this.grpOutput.Location = new System.Drawing.Point(12, 32);
            this.grpOutput.Name = "grpOutput";
            this.grpOutput.Size = new System.Drawing.Size(486, 62);
            this.grpOutput.TabIndex = 5;
            this.grpOutput.TabStop = false;
            this.grpOutput.Text = "Output File";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Location = new System.Drawing.Point(0, 487);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(510, 22);
            this.statusStrip1.TabIndex = 6;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // lblStatus
            // 
            this.lblStatus.AutoSize = true;
            this.lblStatus.Location = new System.Drawing.Point(211, 492);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(52, 13);
            this.lblStatus.TabIndex = 7;
            this.lblStatus.Text = "Waiting...";
            // 
            // grpChoice
            // 
            this.grpChoice.Controls.Add(this.rdAes800);
            this.grpChoice.Controls.Add(this.rdRng);
            this.grpChoice.Location = new System.Drawing.Point(12, 205);
            this.grpChoice.Name = "grpChoice";
            this.grpChoice.Size = new System.Drawing.Size(486, 44);
            this.grpChoice.TabIndex = 8;
            this.grpChoice.TabStop = false;
            this.grpChoice.Text = "Entropy Compare AesCtr With:";
            // 
            // rdAes800
            // 
            this.rdAes800.AutoSize = true;
            this.rdAes800.Location = new System.Drawing.Point(167, 21);
            this.rdAes800.Name = "rdAes800";
            this.rdAes800.Size = new System.Drawing.Size(84, 17);
            this.rdAes800.TabIndex = 1;
            this.rdAes800.Text = "Aes800Drbg";
            this.rdAes800.UseVisualStyleBackColor = true;
            this.rdAes800.CheckedChanged += new System.EventHandler(this.OnCheckChanged);
            // 
            // rdRng
            // 
            this.rdRng.AutoSize = true;
            this.rdRng.Checked = true;
            this.rdRng.Location = new System.Drawing.Point(7, 20);
            this.rdRng.Name = "rdRng";
            this.rdRng.Size = new System.Drawing.Size(154, 17);
            this.rdRng.TabIndex = 0;
            this.rdRng.TabStop = true;
            this.rdRng.Text = "RNGCryptoServiceProvider";
            this.rdRng.UseVisualStyleBackColor = true;
            this.rdRng.CheckedChanged += new System.EventHandler(this.OnCheckChanged);
            // 
            // grpModes
            // 
            this.grpModes.Controls.Add(this.rdCSRG);
            this.grpModes.Controls.Add(this.rdCtrDual);
            this.grpModes.Controls.Add(this.rdCtrDualCha);
            this.grpModes.Controls.Add(this.rdCtr);
            this.grpModes.Controls.Add(this.label5);
            this.grpModes.Controls.Add(this.label3);
            this.grpModes.Controls.Add(this.rdChaotic);
            this.grpModes.Controls.Add(this.rdCtrChain);
            this.grpModes.Controls.Add(this.rdOscillating);
            this.grpModes.Controls.Add(this.rdCycling);
            this.grpModes.Controls.Add(this.rdAutoSeed);
            this.grpModes.Location = new System.Drawing.Point(12, 100);
            this.grpModes.Name = "grpModes";
            this.grpModes.Size = new System.Drawing.Size(486, 99);
            this.grpModes.TabIndex = 9;
            this.grpModes.TabStop = false;
            this.grpModes.Text = "AesCtr Generation Mode";
            // 
            // rdCSRG
            // 
            this.rdCSRG.AutoSize = true;
            this.rdCSRG.Checked = true;
            this.rdCSRG.Location = new System.Drawing.Point(292, 71);
            this.rdCSRG.Name = "rdCSRG";
            this.rdCSRG.Size = new System.Drawing.Size(105, 17);
            this.rdCSRG.TabIndex = 24;
            this.rdCSRG.TabStop = true;
            this.rdCSRG.Text = "CSRG Generator";
            this.rdCSRG.UseVisualStyleBackColor = true;
            // 
            // rdCtrDual
            // 
            this.rdCtrDual.AutoSize = true;
            this.rdCtrDual.Location = new System.Drawing.Point(89, 71);
            this.rdCtrDual.Name = "rdCtrDual";
            this.rdCtrDual.Size = new System.Drawing.Size(72, 17);
            this.rdCtrDual.TabIndex = 23;
            this.rdCtrDual.Text = "Dual CTR";
            this.rdCtrDual.UseVisualStyleBackColor = true;
            // 
            // rdCtrDualCha
            // 
            this.rdCtrDualCha.AutoSize = true;
            this.rdCtrDualCha.Location = new System.Drawing.Point(172, 71);
            this.rdCtrDualCha.Name = "rdCtrDualCha";
            this.rdCtrDualCha.Size = new System.Drawing.Size(111, 17);
            this.rdCtrDualCha.TabIndex = 22;
            this.rdCtrDualCha.Text = "Dual CTR Chaotic";
            this.rdCtrDualCha.UseVisualStyleBackColor = true;
            // 
            // rdCtr
            // 
            this.rdCtr.AutoSize = true;
            this.rdCtr.Location = new System.Drawing.Point(7, 71);
            this.rdCtr.Name = "rdCtr";
            this.rdCtr.Size = new System.Drawing.Size(71, 17);
            this.rdCtr.TabIndex = 21;
            this.rdCtr.Text = "AES CTR";
            this.rdCtr.UseVisualStyleBackColor = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(3, 56);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(78, 12);
            this.label5.TabIndex = 20;
            this.label5.Text = "CTR Methods:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(4, 19);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(79, 12);
            this.label3.TabIndex = 19;
            this.label3.Text = "CBC Methods:";
            // 
            // rdChaotic
            // 
            this.rdChaotic.AutoSize = true;
            this.rdChaotic.Location = new System.Drawing.Point(375, 33);
            this.rdChaotic.Name = "rdChaotic";
            this.rdChaotic.Size = new System.Drawing.Size(106, 17);
            this.rdChaotic.TabIndex = 4;
            this.rdChaotic.Text = "Chaotic Re-Seed";
            this.rdChaotic.UseVisualStyleBackColor = true;
            // 
            // rdCtrChain
            // 
            this.rdCtrChain.AutoSize = true;
            this.rdCtrChain.Location = new System.Drawing.Point(301, 33);
            this.rdCtrChain.Name = "rdCtrChain";
            this.rdCtrChain.Size = new System.Drawing.Size(68, 17);
            this.rdCtrChain.TabIndex = 3;
            this.rdCtrChain.Text = "Ctr Chain";
            this.rdCtrChain.UseVisualStyleBackColor = true;
            // 
            // rdOscillating
            // 
            this.rdOscillating.AutoSize = true;
            this.rdOscillating.Location = new System.Drawing.Point(201, 33);
            this.rdOscillating.Name = "rdOscillating";
            this.rdOscillating.Size = new System.Drawing.Size(94, 17);
            this.rdOscillating.TabIndex = 2;
            this.rdOscillating.Text = "Key Oscillation";
            this.rdOscillating.UseVisualStyleBackColor = true;
            this.rdOscillating.CheckedChanged += new System.EventHandler(this.OnCheckChanged);
            // 
            // rdCycling
            // 
            this.rdCycling.AutoSize = true;
            this.rdCycling.Location = new System.Drawing.Point(88, 33);
            this.rdCycling.Name = "rdCycling";
            this.rdCycling.Size = new System.Drawing.Size(110, 17);
            this.rdCycling.TabIndex = 1;
            this.rdCycling.Text = "Cycling Key Chain";
            this.rdCycling.UseVisualStyleBackColor = true;
            this.rdCycling.CheckedChanged += new System.EventHandler(this.OnCheckChanged);
            // 
            // rdAutoSeed
            // 
            this.rdAutoSeed.AutoSize = true;
            this.rdAutoSeed.Location = new System.Drawing.Point(7, 34);
            this.rdAutoSeed.Name = "rdAutoSeed";
            this.rdAutoSeed.Size = new System.Drawing.Size(75, 17);
            this.rdAutoSeed.TabIndex = 0;
            this.rdAutoSeed.Text = "Auto Seed";
            this.rdAutoSeed.UseVisualStyleBackColor = true;
            this.rdAutoSeed.CheckedChanged += new System.EventHandler(this.OnCheckChanged);
            // 
            // grpParamaters
            // 
            this.grpParamaters.Controls.Add(this.btnOpenFile);
            this.grpParamaters.Controls.Add(this.chkBitmaps);
            this.grpParamaters.Controls.Add(this.chkSummary);
            this.grpParamaters.Controls.Add(this.cbTestIterations);
            this.grpParamaters.Controls.Add(this.label4);
            this.grpParamaters.Controls.Add(this.label2);
            this.grpParamaters.Location = new System.Drawing.Point(11, 255);
            this.grpParamaters.Name = "grpParamaters";
            this.grpParamaters.Size = new System.Drawing.Size(487, 92);
            this.grpParamaters.TabIndex = 10;
            this.grpParamaters.TabStop = false;
            this.grpParamaters.Text = "Test Parameters";
            // 
            // btnOpenFile
            // 
            this.btnOpenFile.Enabled = false;
            this.btnOpenFile.Location = new System.Drawing.Point(406, 30);
            this.btnOpenFile.Name = "btnOpenFile";
            this.btnOpenFile.Size = new System.Drawing.Size(75, 30);
            this.btnOpenFile.TabIndex = 13;
            this.btnOpenFile.Text = "Open File";
            this.btnOpenFile.UseVisualStyleBackColor = true;
            this.btnOpenFile.Click += new System.EventHandler(this.OnOpenFileClick);
            // 
            // chkBitmaps
            // 
            this.chkBitmaps.AutoSize = true;
            this.chkBitmaps.Location = new System.Drawing.Point(137, 69);
            this.chkBitmaps.Name = "chkBitmaps";
            this.chkBitmaps.Size = new System.Drawing.Size(242, 17);
            this.chkBitmaps.TabIndex = 12;
            this.chkBitmaps.Text = "Create Bitmaps of Random at Results location";
            this.chkBitmaps.UseVisualStyleBackColor = true;
            // 
            // chkSummary
            // 
            this.chkSummary.AutoSize = true;
            this.chkSummary.Location = new System.Drawing.Point(8, 69);
            this.chkSummary.Name = "chkSummary";
            this.chkSummary.Size = new System.Drawing.Size(119, 17);
            this.chkSummary.TabIndex = 11;
            this.chkSummary.Text = "Write Summary only";
            this.chkSummary.UseVisualStyleBackColor = true;
            // 
            // cbTestIterations
            // 
            this.cbTestIterations.FormattingEnabled = true;
            this.cbTestIterations.Items.AddRange(new object[] {
            "2 * 50000",
            "10 * 10000",
            "100 * 1000",
            "1000 * 100",
            "10000 * 10"});
            this.cbTestIterations.Location = new System.Drawing.Point(8, 36);
            this.cbTestIterations.Name = "cbTestIterations";
            this.cbTestIterations.Size = new System.Drawing.Size(119, 21);
            this.cbTestIterations.TabIndex = 5;
            this.cbTestIterations.Text = "10 * 10000";
            this.cbTestIterations.SelectedValueChanged += new System.EventHandler(this.OnSelectedValueChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(130, 39);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(136, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "Iterations * Kib (1024 bytes)";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 20);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(57, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Resolution";
            // 
            // lblStatAes
            // 
            this.lblStatAes.AutoSize = true;
            this.lblStatAes.Location = new System.Drawing.Point(12, 463);
            this.lblStatAes.Name = "lblStatAes";
            this.lblStatAes.Size = new System.Drawing.Size(25, 13);
            this.lblStatAes.TabIndex = 11;
            this.lblStatAes.Text = "......";
            // 
            // lblStatOpponent
            // 
            this.lblStatOpponent.AutoSize = true;
            this.lblStatOpponent.Location = new System.Drawing.Point(181, 463);
            this.lblStatOpponent.Name = "lblStatOpponent";
            this.lblStatOpponent.Size = new System.Drawing.Size(25, 13);
            this.lblStatOpponent.TabIndex = 12;
            this.lblStatOpponent.Text = "......";
            // 
            // grpSamples
            // 
            this.grpSamples.Controls.Add(this.lblMib);
            this.grpSamples.Controls.Add(this.lblAlgoSize);
            this.grpSamples.Controls.Add(this.cbAlgoSize);
            this.grpSamples.Controls.Add(this.lblAlgo);
            this.grpSamples.Controls.Add(this.btnSaveAlgo);
            this.grpSamples.Controls.Add(this.cbAlgo);
            this.grpSamples.Location = new System.Drawing.Point(12, 354);
            this.grpSamples.Name = "grpSamples";
            this.grpSamples.Size = new System.Drawing.Size(486, 76);
            this.grpSamples.TabIndex = 13;
            this.grpSamples.TabStop = false;
            this.grpSamples.Text = "Export";
            // 
            // lblMib
            // 
            this.lblMib.AutoSize = true;
            this.lblMib.Location = new System.Drawing.Point(253, 42);
            this.lblMib.Name = "lblMib";
            this.lblMib.Size = new System.Drawing.Size(88, 13);
            this.lblMib.TabIndex = 18;
            this.lblMib.Text = "* 1024000 (1Mib)";
            // 
            // lblAlgoSize
            // 
            this.lblAlgoSize.AutoSize = true;
            this.lblAlgoSize.Location = new System.Drawing.Point(167, 22);
            this.lblAlgoSize.Name = "lblAlgoSize";
            this.lblAlgoSize.Size = new System.Drawing.Size(27, 13);
            this.lblAlgoSize.TabIndex = 17;
            this.lblAlgoSize.Text = "Size";
            // 
            // cbAlgoSize
            // 
            this.cbAlgoSize.FormattingEnabled = true;
            this.cbAlgoSize.Items.AddRange(new object[] {
            "1",
            "10",
            "20",
            "50",
            "100"});
            this.cbAlgoSize.Location = new System.Drawing.Point(167, 38);
            this.cbAlgoSize.Name = "cbAlgoSize";
            this.cbAlgoSize.Size = new System.Drawing.Size(84, 21);
            this.cbAlgoSize.TabIndex = 16;
            this.cbAlgoSize.Text = "10";
            // 
            // lblAlgo
            // 
            this.lblAlgo.AutoSize = true;
            this.lblAlgo.Location = new System.Drawing.Point(9, 23);
            this.lblAlgo.Name = "lblAlgo";
            this.lblAlgo.Size = new System.Drawing.Size(50, 13);
            this.lblAlgo.TabIndex = 15;
            this.lblAlgo.Text = "Algorithm";
            // 
            // btnSaveAlgo
            // 
            this.btnSaveAlgo.Enabled = false;
            this.btnSaveAlgo.Location = new System.Drawing.Point(405, 29);
            this.btnSaveAlgo.Name = "btnSaveAlgo";
            this.btnSaveAlgo.Size = new System.Drawing.Size(75, 30);
            this.btnSaveAlgo.TabIndex = 14;
            this.btnSaveAlgo.Text = "Save File";
            this.btnSaveAlgo.UseVisualStyleBackColor = true;
            this.btnSaveAlgo.Click += new System.EventHandler(this.OnSaveAlgoClick);
            // 
            // cbAlgo
            // 
            this.cbAlgo.FormattingEnabled = true;
            this.cbAlgo.Items.AddRange(new object[] {
            "CSRG",
            "AutoSeed",
            "Block-Chaotic",
            "Block-Chain",
            "Block-Cycling",
            "Block-Oscillating",
            "CTR",
            "CTR-Dual",
            "CTR-Dual-Chaotic",
            "Aes800DRBG"});
            this.cbAlgo.Location = new System.Drawing.Point(9, 38);
            this.cbAlgo.Name = "cbAlgo";
            this.cbAlgo.Size = new System.Drawing.Size(133, 21);
            this.cbAlgo.TabIndex = 0;
            this.cbAlgo.Text = "CSRG";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(510, 509);
            this.Controls.Add(this.grpSamples);
            this.Controls.Add(this.lblStatOpponent);
            this.Controls.Add(this.lblStatAes);
            this.Controls.Add(this.grpParamaters);
            this.Controls.Add(this.grpModes);
            this.Controls.Add(this.grpChoice);
            this.Controls.Add(this.lblStatus);
            this.Controls.Add(this.pbStatus);
            this.Controls.Add(this.btnTestAesCtr);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.grpOutput);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            this.grpOutput.ResumeLayout(false);
            this.grpOutput.PerformLayout();
            this.grpChoice.ResumeLayout(false);
            this.grpChoice.PerformLayout();
            this.grpModes.ResumeLayout(false);
            this.grpModes.PerformLayout();
            this.grpParamaters.ResumeLayout(false);
            this.grpParamaters.PerformLayout();
            this.grpSamples.ResumeLayout(false);
            this.grpSamples.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnTestAesCtr;
        private System.Windows.Forms.Button btnDialog;
        private System.Windows.Forms.TextBox txtOutput;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ProgressBar pbStatus;
        private System.Windows.Forms.GroupBox grpOutput;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.Label lblStatus;
        private System.Windows.Forms.GroupBox grpChoice;
        private System.Windows.Forms.RadioButton rdAes800;
        private System.Windows.Forms.RadioButton rdRng;
        private System.Windows.Forms.GroupBox grpModes;
        private System.Windows.Forms.RadioButton rdOscillating;
        private System.Windows.Forms.RadioButton rdCycling;
        private System.Windows.Forms.RadioButton rdAutoSeed;
        private System.Windows.Forms.GroupBox grpParamaters;
        private System.Windows.Forms.ComboBox cbTestIterations;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckBox chkSummary;
        private System.Windows.Forms.Label lblStatAes;
        private System.Windows.Forms.Label lblStatOpponent;
        private System.Windows.Forms.RadioButton rdCtrChain;
        private System.Windows.Forms.RadioButton rdChaotic;
        private System.Windows.Forms.CheckBox chkBitmaps;
        private System.Windows.Forms.Button btnOpenFile;
        private System.Windows.Forms.GroupBox grpSamples;
        private System.Windows.Forms.Label lblMib;
        private System.Windows.Forms.Label lblAlgoSize;
        private System.Windows.Forms.ComboBox cbAlgoSize;
        private System.Windows.Forms.Label lblAlgo;
        private System.Windows.Forms.Button btnSaveAlgo;
        private System.Windows.Forms.ComboBox cbAlgo;
        private System.Windows.Forms.RadioButton rdCSRG;
        private System.Windows.Forms.RadioButton rdCtrDual;
        private System.Windows.Forms.RadioButton rdCtrDualCha;
        private System.Windows.Forms.RadioButton rdCtr;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label3;
    }
}

