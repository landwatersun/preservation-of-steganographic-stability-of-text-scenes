﻿using System;

/// AES-CTR-DRBG in C# based on the BouncyCastle: http://bouncycastle.org/ java implementation (CTRSP800DRBG.java) of the Nist 80090A standard
/// http://grepcode.com/file/repo1.maven.org/maven2/org.bouncycastle/bcprov-jdk15on/1.49/org/bouncycastle/crypto/prng/drbg/CTRSP800DRBG.java
/// http://csrc.nist.gov/groups/STM/cavp/documents/drbg/DRBGVS.pdf
namespace Drbg_Test
{
    public class Aes800Drbg
    {
        #region Constants
        private const Int32 BLOCK_SIZE = 16;
        private const Int32 KEY_BITS = 256;
        private const Int32 KEY_BYTES = 32;
        private const Int32 IV_BITS = 128;
        private const Int32 IV_BYTES = 16;
        private const Int32 SEED_BYTES = 48;
        private const Int32 SEED_BITS = 384;
        #endregion

        #region Fields
        private AesFastEngine _engine;
        private byte[] _Key;
        private byte[] _IV;
        private static byte[] KBITS = new byte[] { 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f,
			    0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17, 0x18, 0x19, 0x1a, 0x1b, 0x1c, 0x1d, 0x1e, 0x1f };
        #endregion

        #region Properties
        /// <summary>
        /// Seed size
        /// </summary>
        public static int SeedSize
        {
            get { return SEED_BYTES; }
        }
        #endregion

        #region Constructor
        public Aes800Drbg(byte[] Seed)
        {
            if (Seed.Length != SEED_BYTES)
                throw new Exception("Seed length must be 48 bytes!");

            _engine = new AesFastEngine();
            Init(Seed);
        }

        private void Init(byte[] Seed)
        {
            byte[] seed = Derive(Seed);

            _Key = new byte[KEY_BYTES];
            _IV = new byte[BLOCK_SIZE];
            Update(seed, _Key, _IV); 
        }
        #endregion

        private void Update(byte[] Seed, byte[] Key, byte[] Iv)
        {
            byte[] temp = new byte[Seed.Length];
            byte[] outputBlock = new byte[BLOCK_SIZE];
            int i = 0;

            _engine.Init(true, Key);

            while (i * BLOCK_SIZE < Seed.Length)
            {
                Increment(Iv);
                _engine.ProcessBlock(Iv, 0, outputBlock, 0);
                int bytesToCopy = ((temp.Length - i * BLOCK_SIZE) > BLOCK_SIZE) ? BLOCK_SIZE : (temp.Length - i * BLOCK_SIZE);
                Buffer.BlockCopy(outputBlock, 0, temp, i * BLOCK_SIZE, bytesToCopy);
                ++i;
            }

            Xor(temp, Seed, temp, 0);
            Buffer.BlockCopy(temp, 0, Key, 0, Key.Length);
            Buffer.BlockCopy(temp, Key.Length, Iv, 0, Iv.Length);
        }

        private void Xor(byte[] Output, byte[] Data1, byte[] Data2, int Data2Offset)
        {
            for (int i = 0; i < Output.Length; i++) 
                Output[i] = (byte)(Data1[i] ^ Data2[i + Data2Offset]);
        }
    
        private void Increment(byte[] Vector)
        {
            int carry = 1;
            for (int i = 1; i <= Vector.Length; i++)
            {
                int res = (Vector[Vector.Length - i] & 0xff) + carry;
                carry = (res > 0xff) ? 1 : 0;
                Vector[Vector.Length - i] = (byte)res;
            }
        } 

        private byte[] Derive(byte[] SeedData)
        {
            int counter = 0;
            int blockLen = ((SEED_BYTES + BLOCK_SIZE + 8) / BLOCK_SIZE) * BLOCK_SIZE;
            byte[] seed = new byte[blockLen];
            CopyIntToByteArray(seed, SEED_BYTES, 0);
            CopyIntToByteArray(seed, SEED_BYTES, 4);
            Buffer.BlockCopy(SeedData, 0, seed, 8, SEED_BYTES);
            seed[8 + SEED_BYTES] = (byte)0x80;
            byte[] temp = new byte[SEED_BYTES];
            byte[] bccOut = new byte[BLOCK_SIZE];
            byte[] iv = new byte[IV_BYTES]; 
            byte[] key = new byte[KEY_BYTES];
            Buffer.BlockCopy(KBITS, 0, key, 0, key.Length);

            while (counter * IV_BITS < SEED_BITS)
            {
                CopyIntToByteArray(iv, counter, 0);
                BlockChain(bccOut, key, iv, seed);
                int bytesToCopy = ((temp.Length - counter * BLOCK_SIZE) > BLOCK_SIZE) ? BLOCK_SIZE : (temp.Length - counter * BLOCK_SIZE);
                Buffer.BlockCopy(bccOut, 0, temp, counter * BLOCK_SIZE, bytesToCopy);
                ++counter;
            }

            counter = 0;
            byte[] block = new byte[BLOCK_SIZE];
            Buffer.BlockCopy(temp, 0, key, 0, key.Length);
            Buffer.BlockCopy(temp, key.Length, block, 0, block.Length);
            temp = new byte[SEED_BITS / 2];

            _engine.Init(true, key);

            while (counter * BLOCK_SIZE < temp.Length)
            {
                _engine.ProcessBlock(block, 0, block, 0);
                int bytesToCopy = ((temp.Length - counter * BLOCK_SIZE) > BLOCK_SIZE) ? BLOCK_SIZE : (temp.Length - counter * BLOCK_SIZE);
                Buffer.BlockCopy(block, 0, temp, counter * BLOCK_SIZE, bytesToCopy);
                counter++;
            }

            return temp;
        }

        private void BlockChain(byte[] ChainOut, byte[] Key, byte[] Iv, byte[] Data)
        {
            byte[] chainingValue = new byte[BLOCK_SIZE];
            int dataLen = Data.Length / BLOCK_SIZE;
            byte[] inputBlock = new byte[BLOCK_SIZE];

            _engine.Init(true, Key);
            _engine.ProcessBlock(Iv, 0, chainingValue, 0);

            for (int i = 0; i < dataLen; i++)
            {
                Xor(inputBlock, chainingValue, Data, i * BLOCK_SIZE);
                _engine.ProcessBlock(inputBlock, 0, chainingValue, 0);
            }

            Buffer.BlockCopy(chainingValue, 0, ChainOut, 0, ChainOut.Length);
        }

        private void CopyIntToByteArray(byte[] Buffer, int Value, int OffSet)
        {
            Buffer[OffSet + 0] = ((byte)(Value >> 24));
            Buffer[OffSet + 1] = ((byte)(Value >> 16));
            Buffer[OffSet + 2] = ((byte)(Value >> 8));
            Buffer[OffSet + 3] = ((byte)(Value));
        }

        public int Generate(byte[] output, byte[] AdditionalEntropy = null)
        {
            if (AdditionalEntropy != null)
            {
                if (AdditionalEntropy.Length != SEED_BYTES)
                    throw new Exception("AdditionalEntropy length must be 48 bytes!");
                else
                    Init(AdditionalEntropy);
            }

            byte[] bout = new byte[_IV.Length];
            _engine.Init(true, _Key);

            for (int i = 0; i < output.Length; i +=BLOCK_SIZE)
            {
                Increment(_IV);
                _engine.ProcessBlock(_IV, 0, bout, 0);
                Buffer.BlockCopy(bout, 0, output, i, BLOCK_SIZE);
            }

            return output.Length * 8;
        }
    }
}
