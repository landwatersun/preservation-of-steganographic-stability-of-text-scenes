﻿/// An AES-CTR-DRBG implementation..
/// 
/// Chaotic State Random Generator, CSRG (nick. Chaos), v1.0, July 14, 2014
/// Updated July 20, 2014
/// Authored by: John Underhill, steppenwolfe_2000@yahoo.com
/// AES routines based on several implementations including Mono: https://github.com/mono, and BouncyCastle: http://bouncycastle.org/
/// Many thanks to the authors of those great projects, and the authors of the original Novell implementation.. j.u.
/// 
/// The NIST Statistical Test Suite is a great tool, and I found the code to be very impressive and well written, (and the math!). A huge thank you to the authors..
/// The original console suite is available on the NIST website: http://csrc.nist.gov/groups/ST/toolkit/rng/documentation_software.html
/// 
/// Future revisions will be maintained on GitHub: https://github.com/Steppenwolfe65/AES-CTR-DRBG
/// 
/// Licence is free for all use, provided the user accepts that the author, (John Underhill), offers absolutely no support for this software, 
/// makes no claim of fitness or merchantability, and all and any responsibility of any kind, known or unknown, is entirely the responsibility of the user or distributer of this code 
/// or any derivations thereof. I further disavow any and all liability or responsibility in its use, now and for all time. 
/// 
/// This header must remain in any distributions of this class.

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace Drbg_Test
{
    public partial class frmNist : Form
    {
        #region Constants
        private const string DIALOG_DEFAULT = "[Select an Output Folder]";
        private const string RESULTS_NAME = "results.txt";
        private const string RANDOM_NAME = "random";
        private const string RANDOM_QEXT = ".bin";
        private const string LOGFILE_NAME = "results.log";
        #endregion

        #region Enums
        private enum Candidates
        {
            CSRG,
            RNGC,
            P800,
            RORG,
        }

        private enum Samples
        {
            KIB10,
            KIB100,
            MIB1,
            MIB10,
        }
        #endregion

        #region Fields
        private BackgroundWorker _analysisWorker = new BackgroundWorker();
        private BackgroundWorker _buildWorker = new BackgroundWorker();
        private BackgroundWorker _comparativeWorker = new BackgroundWorker();
        private Dictionary<string, string> _testResults = new Dictionary<string, string>();
        private ListViewItem[] _compareItems;
        private Samples _exportSize = Samples.KIB100;
        private Stopwatch _runTimer = new Stopwatch();
        private readonly string[] _compareNames = new string[] { 
            "ApproximateEntropy", 
            "BlockFrequency", 
            "CumulativeSums", 
            "FFT", 
            "Frequency", 
            "LinearComplexity", 
            "LongestRun", 
            "NonOverlappingTemplate", 
            "OverlappingTemplate", 
            "Rank", 
            "Runs", 
            "Serial", 
            };
        #endregion

        #region Properties
        private Candidates Candidate { get; set; }
        private Candidates Opponent { get; set; }
        private string OutputDirectory { get; set; }
        private Samples SampleSize { get; set; }
        private int TestCount { get; set; }
        #endregion

        #region Constructor
        public frmNist()
        {
            InitializeComponent();

            const int SIZE = 10240000;//10Mib
            ParallelTest(SIZE);
            CSRGIntegrityTest();
            SHA256Test();
        }
        #endregion

        #region Controls
        private void OnAnalyzeClick(object sender, EventArgs e)
        {
            // no you don't..
            if (this.TestCount == 0)
            {
                MessageBox.Show("There are no tests selected!");
                return;
            }
            if (!Directory.Exists(OutputDirectory))
            {
                MessageBox.Show("How did that happen?!");
                return;
            }

            // get test params
            SetNistParams();
            // setup progress
            NistSTS.ProgressChanged -= OnNistProgressChanged;
            NistSTS.ProgressChanged += new Action<int, string>(OnNistProgressChanged);
            pbStatus.Value = 0;
            pbStatus.Maximum = this.TestCount + 1;
            // notify
            lblStatus.Text = "Test Started..";
            lblBuildSize.Text = Candidate.ToString() + " Building " + SampleSize.ToString() + "..";
            // clear old data
            _testResults.Clear();
            // disable ui
            EnabledState(false);
            // clear lv
            lvAlgorithmTest.Items.Clear();
            // run it
            _analysisWorker.RunWorkerAsync();
        }

        private void OnBuildClick(object sender, EventArgs e)
        {
            if (!Directory.Exists(OutputDirectory))
            {
                MessageBox.Show("How did that happen?!");
                return;
            }

            // disable ui
            EnabledState(false);
            // time it
            TimerStart();
            // run it
            _buildWorker.RunWorkerAsync();
        }

        private void OnCompareClick(object sender, EventArgs e)
        {
            // where you goin?..
            if (!Directory.Exists(OutputDirectory))
            {
                MessageBox.Show("How did that happen?!");
                return;
            }

            // reset progress bar
            pbStatus.Value = 0;
            pbStatus.Maximum = 12;
            // doing it locally
            NistSTS.ProgressChanged -= OnNistProgressChanged;

            // 10k not large enough for these tests
            NistSTS.ParametersKey[Tests.RandomExcursions] = false;
            NistSTS.ParametersKey[Tests.RandomExcursionsVariant] = false;
            NistSTS.ParametersKey[Tests.Universal] = false;
            this.TestCount = 12;
            
            // clear old
            lvCompare.Items.Clear();
            // disable ui
            EnabledState(false);
            // run it
            _comparativeWorker.RunWorkerAsync();
        }

        private void OnCandidateCheckChanged(object sender, EventArgs e)
        {
            RadioButton rd = sender as RadioButton;
            if (!rd.Checked) return;

            if (rd.Name.Equals("rdCSRG"))
                this.Candidate = Candidates.CSRG;
            if (rd.Name.Equals("rdRng"))
                this.Candidate = Candidates.RNGC;
            if (rd.Name.Equals("rdCTRSP800DRBG"))
                this.Candidate = Candidates.P800;
            if (rd.Name.Equals("rdRandOrg"))
            {
                // site quota 1mb per day
                this.Candidate = Candidates.RORG;
                rd100Kib.Checked = !rd10Kib.Checked ? true : false;
                rd1Mib.Enabled = false;
                rd10Mib.Enabled = false;
            }
            else
            {
                rd1Mib.Enabled = true;
                rd10Mib.Enabled = true;
            }
        }

        private void OnCompareCheckChanged(object sender, EventArgs e)
        {
            RadioButton rd = sender as RadioButton;

            if (rd.Checked)
            {
                if (rd.Text == "RNGCryptoServiceProvider")
                {
                    lblOpponent.Text = "RNGCryptoServiceProvider";
                    this.Opponent = Candidates.RNGC;
                }
                else if (rd.Text == "CTRSP800DRBG")
                {
                    lblOpponent.Text = "CTRSP800DRBG";
                    this.Opponent = Candidates.P800;
                }
            }
        }

        private void OnDialogClick(object sender, EventArgs e)
        {
            txtOutput.Text = DIALOG_DEFAULT;
            lblStatus.Text = "Waiting..";
            pbStatus.Value = 0;

            using (FolderBrowserDialog fbDiag = new FolderBrowserDialog())
            {
                fbDiag.Description = "Select an Output Folder";

                if (fbDiag.ShowDialog() == DialogResult.OK)
                {
                    if (!Utilities.DirectoryIsWritable(fbDiag.SelectedPath))
                    {
                        MessageBox.Show("You do not have permission to create files in this directory! Choose a different path..");
                        OutputDirectory = string.Empty;
                    }
                    else
                    {
                        OutputDirectory = fbDiag.SelectedPath;
                        txtOutput.Text = OutputDirectory;
                        EnabledState(true);
                    }
                }
            }
        }

        private void OnDifferentialChanged(object sender, EventArgs e)
        {
            ComboBox cb = sender as ComboBox;

            if (cb.Text == "32")
                Build.Differential = Differentials.D32;
            else if (cb.Text == "64")
                Build.Differential = Differentials.D64;
            else if (cb.Text == "128")
                Build.Differential = Differentials.D128;
            else if (cb.Text == "512")
                Build.Differential = Differentials.D512;
            else
                Build.Differential = Differentials.D256;
        }

        private void OnEngineChanged(object sender, EventArgs e)
        {
            RadioButton rd = sender as RadioButton;

            if (rd.Checked == false) return;

            if (rd.Name == "rdDual")
                Build.CSRGEngine = Engines.DUAL;
            else
                Build.CSRGEngine = Engines.SINGLE;
        }

        private void OnExportValueChanged(object sender, EventArgs e)
        {
            ComboBox cb = sender as ComboBox;

            if (cb.Text.Contains("10Kib"))
                _exportSize = Samples.KIB10;
            else if (cb.Text.Contains("1Mib"))
                _exportSize = Samples.MIB1;
            else if (cb.Text.Contains("10Mib"))
                _exportSize = Samples.MIB10;
            else
                _exportSize = Samples.KIB100;
        }

        private void OnFormClose(object sender, FormClosingEventArgs e)
        {
            _analysisWorker.Dispose();
            _buildWorker.Dispose();
            _comparativeWorker.Dispose();
            SaveSettings();
        }

        private void OnFormHelpClicked(object sender, CancelEventArgs e)
        {
            frmHelp f = new frmHelp();
            f.ShowDialog(this);
            // get rid of the help icon
            DoMouseClick();
        }

        private void OnFormLoad(object sender, EventArgs e)
        {
            this.SampleSize = Samples.KIB100;
            this.Candidate = Candidates.CSRG;
            this.Opponent = Candidates.RNGC;
            this.TestCount = 15;
            LoadSettings();
            cbDifferential.SelectedIndex = 3;
            cbBuildSize.SelectedIndex = 3;
            EnabledState(false);
            grpOutput.Enabled = true;

            // init the background threads
            _analysisWorker.DoWork += OnAnalysisDoWork;
            _analysisWorker.RunWorkerCompleted += OnAnalysisWorkCompleted;
            _buildWorker.DoWork += OnBuildDoWork;
            _buildWorker.RunWorkerCompleted += OnBuildWorkCompleted;
            _comparativeWorker.DoWork += OnComparativeDoWork;
            _comparativeWorker.RunWorkerCompleted += OnComparativeWorkCompleted;
        }

        private void OnNistProgressChanged(int count, string message)
        {
            pbStatus.Invoke(new MethodInvoker(delegate { pbStatus.Value = (pbStatus.Value == pbStatus.Maximum) ? 0 : pbStatus.Value + 1; }));
            lblStatus.Invoke(new MethodInvoker(delegate { lblStatus.Text = message; }));
        }

        private void OnSampleSizeCheckChanged(object sender, EventArgs e)
        {
            RadioButton rd = sender as RadioButton;
            if (!rd.Checked) return;

            if (rd.Name.Equals("rd10Kib"))
                this.SampleSize = Samples.KIB10;
            if (rd.Name.Equals("rd100Kib"))
                this.SampleSize = Samples.KIB100;
            if (rd.Name.Equals("rd1Mib"))
                this.SampleSize = Samples.MIB1;
            if (rd.Name.Equals("rd10Mib"))
                this.SampleSize = Samples.MIB10;

            // tests require +1Mib
            if (rd.Name.Equals("rd10Kib") || rd.Name.Equals("rd100Kib"))
            {
                RandomExcursions.Enabled = false;
                RandomExcursionsVariant.Enabled = false;
                Universal.Enabled = false;
            }
            else
            {
                RandomExcursions.Enabled = true;
                RandomExcursionsVariant.Enabled = true;
                Universal.Enabled = true;
            }
        }

        private void OnTestCountChanged(object sender, EventArgs e)
        {
            this.TestCount = 0;

            foreach (CheckBox chk in ((Control)grpTests).Controls)
                if (chk.Checked && chk.Enabled)
                    this.TestCount++;
        }
        #endregion

        #region Threaded Responses
        private void OnAnalysisDoWork(object sender, DoWorkEventArgs e)
        {
            string tmpFile = Utilities.GetUniquePath(OutputDirectory, RANDOM_NAME, RANDOM_QEXT);

            switch (this.Candidate)
            {
                case Candidates.CSRG:
                    {
                        if (SampleSize == Samples.KIB10)
                            Build.CSRGBuild10KB(tmpFile);
                        else if (SampleSize == Samples.KIB100)
                            Build.CSRGBuild100KB(tmpFile);
                        else if (SampleSize == Samples.MIB1)
                            Build.CSRGBuild1MB(tmpFile);
                        else if (SampleSize == Samples.MIB10)
                            Build.CSRGBuild10MB(tmpFile);
                    }
                    break;
                case Candidates.P800:
                    {
                        if (SampleSize == Samples.KIB10)
                            Build.DRBG800Build10KB(tmpFile);
                        else if (SampleSize == Samples.KIB100)
                            Build.DRBG800Build100KB(tmpFile);
                        else if (SampleSize == Samples.MIB1)
                            Build.DRBG800Build1MB(tmpFile);
                        else if (SampleSize == Samples.MIB10)
                            Build.DRBG800Build10MB(tmpFile);
                    }
                    break;
                case Candidates.RNGC:
                    {
                        // unseeded algo
                        int size = 10240;

                        if (SampleSize == Samples.KIB100)
                            size = 102400;
                        else if (SampleSize == Samples.MIB1)
                            size = 1024000;
                        else if (SampleSize == Samples.MIB10)
                            size = 10240000;
                        Build.RNGCBuild(tmpFile, size);
                    }
                    break;
                case Candidates.RORG:
                    {
                        // test for site quota failure
                        try
                        {
                            if (SampleSize == Samples.KIB10)
                                Build.ROrgBuild10KB(tmpFile);
                            else if (SampleSize == Samples.KIB100)
                                Build.ROrgBuild100KB(tmpFile);
                        }
                        catch
                        {
                            // no more for today! Support the site. Buy some credits..
                            MessageBox.Show("Random.org: You have reached the Site Quota for random for today! Try again in 24 hours.");
                            rdCSRG.Invoke(new MethodInvoker(delegate { rdCSRG.Checked = true; }));
                            rdRandOrg.Invoke(new MethodInvoker(delegate { rdRandOrg.Enabled = false; }));
                            return;
                        }
                    }
                    break;
            }

            // gets 1 for the build
            pbStatus.Invoke(new MethodInvoker(delegate { pbStatus.Value = 1; }));
            // update ui
            lblStatus.Invoke(new MethodInvoker(delegate { lblStatus.Text = "Build Completed. Starting analysis.."; }));
            lblBuildSize.Invoke(new MethodInvoker(delegate { lblBuildSize.Text = Candidate.ToString() + " Built " + SampleSize.ToString(); }));

            // get the stats
            _testResults = NistSTS.Run(tmpFile);

            // delete temp
            if (File.Exists(tmpFile))
                File.Delete(tmpFile);
        }

        private void OnAnalysisWorkCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            // return ui
            EnabledState(true);
            pbStatus.Value = pbStatus.Maximum;

            // whoops
            if (_testResults.Count < 1)
            {
                lblStatus.Text = "The test Failed!";
                return;
            }

            // put the values to list
            foreach (var val in _testResults)
            {
                ListViewItem item = new ListViewItem(val.Key);
                string[] sub = val.Value.Split(',');

                if (sub.Length > 1)
                {
                    item.SubItems.Add(sub[0]);
                    item.SubItems.Add(sub[1]);
                    lvAlgorithmTest.Items.Add(item);
                }
            }
            // yay!
            lblStatus.Text = "Test Complete!";

            if (chkLogResults.Checked)
                AnalysisToLog();
        }

        private void OnBuildDoWork(object sender, DoWorkEventArgs e)
        {
            string randPath = Utilities.GetUniquePath(this.OutputDirectory, RANDOM_NAME, RANDOM_QEXT);

            if (_exportSize == Samples.KIB10)
                Build.CSRGBuild10KB(randPath);
            else if (_exportSize == Samples.MIB1)
                Build.CSRGBuild1MB(randPath);
            else if (_exportSize == Samples.MIB10)
                Build.CSRGBuild10MB(randPath);
            else 
                Build.CSRGBuild100KB(randPath);
        }

        private void OnBuildWorkCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            // return ui
            EnabledState(true);
            // stats
            lblBuildSize.Text = "Completed in " + TimerStop().ToString(@"m\:ss\.ff") + " seconds";
            lblStatus.Text = "Test Complete!";
        }

        private void OnComparativeDoWork(object sender, DoWorkEventArgs e)
        {
            Dictionary<int, Dictionary<string, string>> results = new Dictionary<int, Dictionary<string, string>>();
            Dictionary<int, Dictionary<string, string>> averages = new Dictionary<int, Dictionary<string, string>>();
            string tmpFile1 = Utilities.GetUniquePath(this.OutputDirectory, RANDOM_NAME, RANDOM_QEXT);
            string tmpFile2 = Utilities.GetUniquePath(this.OutputDirectory, RANDOM_NAME, RANDOM_QEXT);
            int ct = 0;

            // build and get p-values for 2 * 10 runs
            for (int i = 0; i < 10; i++)
            {
                OnNistProgressChanged(1, "Building data..");

                Build.CSRGBuild10KB(tmpFile1);
                // add to results
                results.Add(ct++, NistSTS.Run(tmpFile1));

                if (File.Exists(tmpFile1))
                    File.Delete(tmpFile1);

                if (this.Opponent == Candidates.P800)
                    Build.DRBG800Build10KB(tmpFile2);
                else
                    Build.RNGCBuild(tmpFile2, 10240);
                // add it every 2nd item
                results.Add(ct++, NistSTS.Run(tmpFile2));

                if (File.Exists(tmpFile2))
                    File.Delete(tmpFile2);
            }

            int lp = 0;
            string sval = "";
            double pval = 0.0;
            OnNistProgressChanged(1, "Collecting statistics..");

            // collect the p-values 
            for (int i = 0; i < this.TestCount; i++)
            {
                string sname = _compareNames[i];
                double[] pvals1 = new double[10];
                double[] pvals2 = new double[10];
                ct = 0;
                int it1 = 0;
                int it2 = 0;

                // calculate the averages
                foreach (var dic in results)
                {
                    if (ct % 2 == 0)
                    {
                        sval = dic.Value[sname];
                        pval = 0.0;
                        string[] sub = sval.Split(',');
                        double.TryParse(sub[0], out pval);
                        pvals1[it1++] = pval;
                    }
                    else
                    {
                        sval = dic.Value[sname];
                        pval = 0.0;
                        string[] sub = sval.Split(',');
                        double.TryParse(sub[0], out pval);
                        pvals2[it2++] = pval;
                    }

                    ct++;
                }
                // add to averages
                averages.Add(lp++, NistSTS.Average(sname, pvals1));
                averages.Add(lp++, NistSTS.Average(sname, pvals2));
            }

            ct = 0;
            // create the lv items
            _compareItems = new ListViewItem[this.TestCount];
            for (int i = 0; i < this.TestCount; i++)
            {
                _compareItems[i] = new ListViewItem(averages[ct]["Name"]);
                _compareItems[i].SubItems.Add(averages[ct]["Average"]);
                _compareItems[i].SubItems.Add(averages[ct]["Low"]);
                _compareItems[i].SubItems.Add(averages[ct++]["High"]);
                _compareItems[i].SubItems.Add(averages[ct]["Average"]);
                _compareItems[i].SubItems.Add(averages[ct]["Low"]);
                _compareItems[i].SubItems.Add(averages[ct++]["High"]);
            }
        }

        private void OnComparativeWorkCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            //listView1.Items[1].UseItemStyleForSubItems = false;
            lvCompare.Items.AddRange(_compareItems);
            // return ui
            EnabledState(true);
            pbStatus.Value = pbStatus.Maximum;
            // update
            OnNistProgressChanged(1, "Comparison Completed!");
            // reset test params
            SetNistParams();
            lblStatus.Text = "Comparison Completed!";

            if (chkLogResults.Checked)
                ComparisonToLog();
        }
        #endregion

        #region Helpers
        private void AnalysisToLog()
        {
            string logPath = Path.Combine(this.OutputDirectory, LOGFILE_NAME);
            const string HEADER = "########################## ANALYSIS RESULTS ##########################";

            using (StreamWriter writer = File.AppendText(logPath))
            {
                writer.WriteLine(HEADER);
                writer.WriteLine("Recorded at: " + DateTime.Now.ToLongDateString() + " : " + DateTime.Now.ToLongTimeString());
                writer.WriteLine("Testing: " + this.Candidate.ToString() + " at " + SampleSize.ToString());
                for (int i = 0; i < lvAlgorithmTest.Items.Count; i++)
                {
                    writer.WriteLine(lvAlgorithmTest.Items[i].Text +
                        " P-Value: " + lvAlgorithmTest.Items[i].SubItems[1].Text +
                        " State: " + lvAlgorithmTest.Items[i].SubItems[2].Text);
                }
                writer.WriteLine("");
            }
        }

        private void ComparisonToLog()
        {
            string logPath = Path.Combine(this.OutputDirectory, LOGFILE_NAME);
            const string HEADER = "########################## COMPARISON RESULTS ##########################";

            using (StreamWriter writer = File.AppendText(logPath))
            {
                writer.WriteLine(HEADER);
                writer.WriteLine("Recorded at: " + DateTime.Now.ToLongDateString() + " : " + DateTime.Now.ToLongTimeString());
                for (int i = 0; i < lvCompare.Items.Count; i++)
                {
                    writer.WriteLine(lvCompare.Items[i].Text);
                    writer.WriteLine("CSRG: " +
                        " Avg: " + lvCompare.Items[i].SubItems[1].Text +
                        " Low: " + lvCompare.Items[i].SubItems[2].Text +
                        " High: " + lvCompare.Items[i].SubItems[3].Text);
                    writer.WriteLine(this.Opponent.ToString() + ": " +
                        " Avg: " + lvCompare.Items[i].SubItems[4].Text +
                        " Low: " + lvCompare.Items[i].SubItems[5].Text +
                        " High: " + lvCompare.Items[i].SubItems[6].Text);
                }
                writer.WriteLine("");
            }
        }

        private void EnabledState(bool Enabled)
        {
            grpAlgorithm.Enabled = Enabled;
            grpBuild.Enabled = Enabled;
            grpCompare.Enabled = Enabled;
            grpOptions.Enabled = Enabled;
            grpOutput.Enabled = Enabled;
            grpTests.Enabled = Enabled;
        }

        private void SetNistParams()
        {
            this.TestCount = 0;

            // simple is good..
            foreach (CheckBox chk in ((Control)grpTests).Controls)
            {
                if (chk.Checked && chk.Enabled)
                    this.TestCount++;

                // checkbox name is a member name
                Tests test = (Tests)Enum.Parse(typeof(Tests), chk.Name);
                NistSTS.ParametersKey[test] = chk.Checked && chk.Enabled;
            }
        }

        private void TimerStart()
        {
            // reset and start
            _runTimer.Reset();
            _runTimer.Start();
        }

        private TimeSpan TimerStop()
        {
            // get results and reset
            _runTimer.Stop();
            TimeSpan time = TimeSpan.FromMilliseconds(_runTimer.Elapsed.Milliseconds);
            _runTimer.Reset();
            return time;
        }
        #endregion

        #region Settings
        private void LoadSettings()
        {
            ApproximateEntropy.Checked = Properties.Settings.Default.SettingApproximateEntropy;
            Rank.Checked = Properties.Settings.Default.SettingRank;
            CumulativeSums.Checked = Properties.Settings.Default.SettingCumulativeSums;
            FFT.Checked = Properties.Settings.Default.SettingFFT;
            Frequency.Checked = Properties.Settings.Default.SettingFrequency;
            LinearComplexity.Checked = Properties.Settings.Default.SettingLinearComplexity;
            LongestRun.Checked = Properties.Settings.Default.SettingLongestRun;
            NonOverlappingTemplate.Checked = Properties.Settings.Default.SettingNonOverlappingTemplate;
            OverlappingTemplate.Checked = Properties.Settings.Default.SettingOverlappingTemplate;
            RandomExcursions.Checked = Properties.Settings.Default.SettingRandomExcursions;
            RandomExcursionsVariant.Checked = Properties.Settings.Default.SettingRandomExcursionsVariant;
            Runs.Checked = Properties.Settings.Default.SettingRuns;
            Serial.Checked = Properties.Settings.Default.SettingSerial;
            Universal.Checked = Properties.Settings.Default.SettingUniversal;
            chkLogResults.Checked = Properties.Settings.Default.SettingLogResults;
        }

        private void SaveSettings()
        {
            Properties.Settings.Default.SettingApproximateEntropy = ApproximateEntropy.Checked;
            Properties.Settings.Default.SettingRank = Rank.Checked;
            Properties.Settings.Default.SettingCumulativeSums = CumulativeSums.Checked;
            Properties.Settings.Default.SettingFFT = FFT.Checked;
            Properties.Settings.Default.SettingFrequency = Frequency.Checked;
            Properties.Settings.Default.SettingLinearComplexity = LinearComplexity.Checked;
            Properties.Settings.Default.SettingLongestRun = LongestRun.Checked;
            Properties.Settings.Default.SettingNonOverlappingTemplate = NonOverlappingTemplate.Checked;
            Properties.Settings.Default.SettingOverlappingTemplate = OverlappingTemplate.Checked;
            Properties.Settings.Default.SettingRandomExcursions = RandomExcursions.Checked;
            Properties.Settings.Default.SettingRandomExcursionsVariant = RandomExcursionsVariant.Checked;
            Properties.Settings.Default.SettingRuns = Runs.Checked;
            Properties.Settings.Default.SettingSerial = Serial.Checked;
            Properties.Settings.Default.SettingUniversal = Universal.Checked;
            Properties.Settings.Default.SettingLogResults = chkLogResults.Checked;
            Properties.Settings.Default.Save();
        }
        #endregion

        #region Help Icon
        // gets rid of ? icon after help loads, (know a better way?)..
        [DllImport("user32.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
        public static extern void mouse_event(int dwFlags, int dx, int dy, uint cButtons, uint dwExtraInfo);

        private const int MOUSEEVENTF_LEFTDOWN = 0x02;
        private const int MOUSEEVENTF_LEFTUP = 0x04;
        private const int MOUSEEVENTF_RIGHTDOWN = 0x08;
        private const int MOUSEEVENTF_RIGHTUP = 0x10;

        private void DoMouseClick()
        {
            mouse_event(MOUSEEVENTF_LEFTDOWN | MOUSEEVENTF_LEFTUP, 1, 1, 0, 0);
        }
        #endregion

        #region Tests
        private void CSRGIntegrityTest()
        {
            byte[] data1 = new byte[1024000];
            byte[] data2 = new byte[1024000];
            byte[] seed = CSPRNG.GetSeed96();

            Console.WriteLine("CSRG Equality Tests:");

            // test dual
            using (CSRG csrg = new CSRG())
                data1 = csrg.Generate(102400, seed);
            using (CSRG csrg = new CSRG())
                data2 = csrg.Generate(102400, seed);

            if (data1.SequenceEqual(data2))
                Console.WriteLine("Dual Mode Integrity Test: Passed!");
            else
                Console.WriteLine("Dual Mode Integrity Test: Failed!");

            // test single
            using (CSRG csrg = new CSRG())
                data1 = csrg.Generate(102400, seed, Engines.SINGLE);
            using (CSRG csrg = new CSRG())
                data2 = csrg.Generate(102400, seed, Engines.SINGLE);

            if (data1.SequenceEqual(data2))
                Console.WriteLine("Single Mode Integrity Test: Passed!");
            else
                Console.WriteLine("Single Mode Integrity Test: Failed!");
        }

        private void ParallelTest(int Size)
        {
            int BLOCK_SIZE = 102400; //100kib
            int count = Size / BLOCK_SIZE;
            byte[] key = CSPRNG.GetSeed48();
            byte[][] seeds = new byte[count][];
            Stopwatch runTimer = new Stopwatch();

            for (int i = 0; i < count; i++)
                seeds[i] = CSPRNG.GetSeed96();

            runTimer.Start();
            // linear run
            byte[] ret1 = GetBlocks(Size, key, seeds, Engines.DUAL, Differentials.D256);

            runTimer.Stop();
            TimeSpan ts1 = TimeSpan.FromMilliseconds(runTimer.ElapsedMilliseconds);
            Console.WriteLine("10Mib with Linear in " + ts1.ToString("mm\\:ss\\.ff") + " seconds");

            runTimer.Reset();
            runTimer.Start();

            // parallel run
            byte[] ret2 = GetBlocksParallel(Size, key, seeds, Engines.DUAL, Differentials.D256);

            runTimer.Stop();
            ts1 = TimeSpan.FromMilliseconds(runTimer.ElapsedMilliseconds);
            Console.WriteLine("10Mib with Parallel in " + ts1.ToString("mm\\:ss\\.ff") + " seconds");

            if (!ret1.SequenceEqual(ret2))
                Console.WriteLine("Parallel Test 1 Failed! Arrays are not equal!");
            else
                Console.WriteLine("Parallel Test 1 Passed!");
        }

        private byte[] GetBlocks(int Length, byte[] Key, byte[][] Seeds, Engines engine, Differentials differential)
        {
            int BLOCK_SIZE = 102400;
            int count = Length / BLOCK_SIZE;
            byte[] data = new byte[Length];

            // loop through new data
            for (int i = 0; i < count; i++)
            {
                using (CSRG csrg = new CSRG(Key))
                    Buffer.BlockCopy(csrg.Generate(BLOCK_SIZE, Seeds[i], engine, differential), 0, data, i * BLOCK_SIZE, BLOCK_SIZE);//156,166
            }
            return data;
        }

        private byte[] GetBlocksParallel(int Length, byte[] Key, byte[][] Seeds, Engines engine, Differentials differential)
        {
            int BLOCK_SIZE = 102400;
            int count = Length / BLOCK_SIZE;
            byte[] data = new byte[Length];

            // loop through new data
            System.Threading.Tasks.Parallel.For(0, count, i =>
            {
                using (CSRG csrg = new CSRG(Key))
                    Buffer.BlockCopy(csrg.Generate(BLOCK_SIZE, Seeds[i], engine, differential), 0, data, i * BLOCK_SIZE, BLOCK_SIZE);
            });

            return data;
        }

        private void SHA256Test()
        {
            byte[] data = Build.CSRGBuild1MB();
            byte[] ret1 = new byte[32];
            byte[] ret2 = new byte[32];

            using (SHA256Digest d = new SHA256Digest())
                ret1 = d.ComputeHash(data);
            using (System.Security.Cryptography.SHA256 shaHash = System.Security.Cryptography.SHA256Managed.Create())
                ret2 = shaHash.ComputeHash(data);

            if (!ret1.SequenceEqual(ret2))
                Console.WriteLine("SHA Equality Test 1 Failed! Arrays are not equal!");
            else
                Console.WriteLine("SHA Equality Test 1 Passed!");
        }
        #endregion
    }
}
