﻿using System;
using System.IO;
using System.Net;
using System.Security.Cryptography;

namespace Drbg_Test
{
    internal static class Build
    {
        #region Constants
        private const int BLOCK10 = 10240;
        private const int BLOCK100 = 102400;
        #endregion

        #region Constructor
        static Build()
        {
            CSRGEngine = Engines.DUAL;
            Differential = Differentials.D256;
        }
        #endregion

        #region Properties
        public static Engines CSRGEngine { get; set; }
        public static Differentials Differential { get; set; }
        #endregion

        #region Algorithms
        /// <summary>
        /// Get random using the bouncycastle CTRSP800DRBG algorithm 
        /// </summary>
        public static byte[] GetCTRSP800DRBG(int Size)
        {
            byte[] data = new byte[Size];
            byte[] seed = CSPRNG.GetSeed64();
            byte[] seed2 = CSPRNG.GetSeed64();
            CTRSP800DRBG ctr800 = new CTRSP800DRBG(256, seed);
            ctr800.generate(data, seed2);

            return data;
        }

        /// <summary>
        /// Use the CSRG to generate a random block.
        /// </summary>
        public static byte[] GetCSRG(int Size)
        {
            byte[] data = new byte[Size];
            byte[] tableKey = CSPRNG.GetSeed48();

            using (CSRG random = new CSRG(tableKey))
                data = random.Generate(Size, CSPRNG.GetSeed96(), CSRGEngine, Differential);

            return data;
        }

        /// <summary>
        /// Use the RNGCryptoServiceProvider to generate a random block.
        /// </summary>
        public static byte[] GetRngCrypto(int Size)
        {
            byte[] data = new byte[Size];

            using (RNGCryptoServiceProvider  rng = new RNGCryptoServiceProvider())
                rng.GetBytes(data);

            return data;
        }

        /// <summary>
        /// Use the Random.org to generate a random block.
        /// </summary>
        public static byte[] GetRorg(int Size)
        {
            if (Size > 1000000)
                throw new Exception("Site Limit Exceeded! Limit of 1 million bytes per day from random.org");

            byte[] data = new byte[Size];
            string tmpFile = Path.GetTempFileName();
            string RANDORG_URL = @"http://random.org/cgi-bin/randbyte?nbytes=" + Size.ToString() + "&format=file";

            using (var client = new WebClient())
                client.DownloadFile(RANDORG_URL, tmpFile);

            if (Utilities.GetFileSize(tmpFile) < Size)
                throw new Exception("Site Limit Exceeded! Limit of 1 million bytes per day from random.org");

            using (BinaryReader inputReader = new BinaryReader(new FileStream(tmpFile, FileMode.Open)))
                data = inputReader.ReadBytes(Size);

            return data;
        }
        #endregion

        #region Build Routines
        #region CSRG Builds
        /// <summary>
        /// Build 10Kib of random using CSRG to bytes
        /// </summary>
        public static byte[] CSRGBuild1KB()
        {
            const int SBLOCK = 1024;

            return GetCSRG(SBLOCK);
        }

        /// <summary>
        /// Build 10Kib of random using CSRG to bytes
        /// </summary>
        public static byte[] CSRGBuild10KB()
        {
            return GetCSRG(BLOCK10);
        }

        /// <summary>
        /// Build 10Kib of random using CSRG
        /// </summary>
        /// <param name="FilePath">Full path to file</param>
        public static void CSRGBuild10KB(string FilePath)
        {
            if (File.Exists(FilePath))
                throw (new Exception("The File Exists!"));

            using (BinaryWriter outputWriter = new BinaryWriter(new FileStream(FilePath, FileMode.Create, FileAccess.Write, FileShare.Read)))
                outputWriter.Write(GetCSRG(BLOCK10));
        }

        /// <summary>
        /// Build 10Kib of random using CSRG to bytes
        /// </summary>
        public static byte[] CSRGBuild100KB()
        {
            return GetCSRG(BLOCK100);
        }

        /// <summary>
        /// Build 100Kib of random using CSRG
        /// </summary>
        /// <param name="FilePath">Full path to file</param>
        public static void CSRGBuild100KB(string FilePath)
        {
            if (File.Exists(FilePath))
                throw (new Exception("The File Exists!"));

            using (BinaryWriter outputWriter = new BinaryWriter(new FileStream(FilePath, FileMode.Create, FileAccess.Write, FileShare.Read)))
                outputWriter.Write(GetCSRG(BLOCK100));
        }

        /// <summary>
        /// Build 10Kib of random using CSRG to bytes
        /// </summary>
        public static byte[] CSRGBuild1MB()
        {
            byte[] data = new byte[1024000];

            for (int i = 0; i < 10; i++)
                Buffer.BlockCopy(GetCSRG(BLOCK100), 0, data, i * BLOCK100, BLOCK100);

            return data;
        }

        /// <summary>
        /// Build 1Mib of random using CSRG
        /// </summary>
        /// <param name="FilePath">Full path to file</param>
        public static void CSRGBuild1MB(string FilePath)
        {
            if (File.Exists(FilePath))
                throw (new Exception("The File Exists!"));

            using (BinaryWriter outputWriter = new BinaryWriter(new FileStream(FilePath, FileMode.Create, FileAccess.Write, FileShare.Read)))
            {
                for (int i = 0; i < 10; i++)
                    outputWriter.Write(GetCSRG(BLOCK100));
            }
        }

        /// <summary>
        /// Build 10Kib of random using CSRG to bytes
        /// </summary>
        public static byte[] CSRGBuild10MB()
        {
            byte[] data = new byte[10240000];

            for (int i = 0; i < 100; i++)
                Buffer.BlockCopy(GetCSRG(BLOCK100), 0, data, i * BLOCK100, BLOCK100);

            return data;
        }

        /// <summary>
        /// Build a 10 Mib block using CSRG
        /// </summary>
        /// <param name="FilePath">Full path to file</param>
        public static void CSRGBuild10MB(string FilePath)
        {
            if (File.Exists(FilePath))
                throw (new Exception("The File Exists!"));

            using (BinaryWriter outputWriter = new BinaryWriter(new FileStream(FilePath, FileMode.Create, FileAccess.Write, FileShare.Read)))
            {
                for (int i = 0; i < 100; i++)
                    outputWriter.Write(GetCSRG(BLOCK100));
            }
        }

        /// <summary>
        /// Build 10 * 10Mib of random using CSRG
        /// </summary>
        public static void CSRGBuild10MBx10(string FilePath)
        {
            if (File.Exists(FilePath))
                throw (new Exception("The File Exists!"));

            for (int i = 0; i < 10; i++)
                CSRGBuild10MB(FilePath);
        }
        #endregion

        #region DRBG800 Builds
        /// <summary>
        /// Build 10Kib of random using 80090DRBG
        /// </summary>
        /// <param name="FilePath">Full path to file</param>
        public static void DRBG800Build10KB(string FilePath)
        {
            if (File.Exists(FilePath))
                throw (new Exception("The File Exists!"));

            using (BinaryWriter outputWriter = new BinaryWriter(new FileStream(FilePath, FileMode.Create, FileAccess.Write, FileShare.Read)))
            {
                outputWriter.Write(GetCTRSP800DRBG(BLOCK10));
            }
        }

        /// <summary>
        /// Build 100Kib of random using 80090DRBG
        /// </summary>
        public static byte[] DRBG800Build10KB()
        {
            return GetCTRSP800DRBG(BLOCK10);
        }

        /// <summary>
        /// Build 100Kib of random using 80090DRBG
        /// </summary>
        /// <param name="FilePath">Full path to file</param>
        public static void DRBG800Build100KB(string FilePath)
        {
            if (File.Exists(FilePath))
                throw (new Exception("The File Exists!"));

            using (BinaryWriter outputWriter = new BinaryWriter(new FileStream(FilePath, FileMode.Create, FileAccess.Write, FileShare.Read)))
            {
                for (int i = 0; i < 10; i++)
                    outputWriter.Write(GetCTRSP800DRBG(BLOCK10));
            }
        }

        /// <summary>
        /// Build 1Mib of random using 80090DRBG
        /// </summary>
        /// <param name="FilePath">Full path to file</param>
        public static void DRBG800Build1MB(string FilePath)
        {
            if (File.Exists(FilePath))
                throw (new Exception("The File Exists!"));

            using (BinaryWriter outputWriter = new BinaryWriter(new FileStream(FilePath, FileMode.Create, FileAccess.Write, FileShare.Read)))
            {
                for (int i = 0; i < 100; i++)
                    outputWriter.Write(GetCTRSP800DRBG(BLOCK10));
            }
        }

        /// <summary>
        /// Build 10Mib of random using 80090DRBG
        /// </summary>
        /// <param name="FilePath">Full path to file</param>
        public static void DRBG800Build10MB(string FilePath)
        {
            if (File.Exists(FilePath))
                throw (new Exception("The File Exists!"));

            using (BinaryWriter outputWriter = new BinaryWriter(new FileStream(FilePath, FileMode.Create, FileAccess.Write, FileShare.Read)))
            {
                for (int i = 0; i < 1000; i++)
                    outputWriter.Write(GetCTRSP800DRBG(BLOCK10));
            }
        }
        #endregion

        #region Random.org Builds
        /// <summary>
        /// Build 10Kib of random using Random.org data
        /// </summary>
        /// <param name="FilePath">Full path to file</param>
        public static void ROrgBuild10KB(string FilePath)
        {
            if (File.Exists(FilePath))
                throw (new Exception("The File Exists!"));

            const string RANDORG_URL = @"http://random.org/cgi-bin/randbyte?nbytes=10240&format=file";

            using (var client = new WebClient())
                client.DownloadFile(RANDORG_URL, FilePath);

            if ((int)Utilities.GetFileSize(FilePath) < BLOCK10)
                throw new Exception("Site limit reached!");
        }

        /// <summary>
        /// Build 100Kib of random using data from random.org.
        /// You'll only be able to run this a few times a day! random.org Site Quotas..
        /// </summary>
        /// <param name="FilePath">Full path to file</param>
        public static void ROrgBuild100KB(string FilePath)
        {
            if (File.Exists(FilePath))
                throw (new Exception("The File Exists!"));

            int counter = 0;
            int max = 0;
            int size = 0;

            const int SAMPLE = 102400;
            const string RANDORG_URL = @"http://random.org/cgi-bin/randbyte?nbytes=10240&format=file";

            using (BinaryWriter outputWriter = new BinaryWriter(File.Open(FilePath, FileMode.Create)))
            {
                while (counter < SAMPLE)
                {
                    if (max++ > 20) break;
                    string tmpFile = Path.GetTempFileName();

                    using (var client = new WebClient())
                        client.DownloadFile(RANDORG_URL, tmpFile);

                    if ((int)Utilities.GetFileSize(FilePath) < 10240)
                        throw new Exception("Site limit reached!");

                    using (BinaryReader inputReader = new BinaryReader(new FileStream(tmpFile, FileMode.Open)))
                    {
                        size = (int)inputReader.BaseStream.Length;
                        size = counter + size > SAMPLE ? size - (counter % size) : size;
                        outputWriter.Write(inputReader.ReadBytes(size));
                        counter += size;
                    }

                    if (File.Exists(tmpFile))
                        File.Delete(tmpFile);
                }
            }

        }
        #endregion

        #region RngCrypto
        public static void RNGCBuild(string FilePath, int Size)
        {
            if (File.Exists(FilePath))
                throw (new Exception("The File Exists!"));

            using (BinaryWriter outputWriter = new BinaryWriter(new FileStream(FilePath, FileMode.Create, FileAccess.Write, FileShare.Read)))
                outputWriter.Write(GetRngCrypto(Size));
        }
        #endregion

        #region Compound Builds
        /// <summary>
        /// Build for all three 10k builds at once
        /// </summary>
        public static void Build10KBx10(string RootPath)
        {
            for (int i = 0; i < 10; i++)
                CSRGBuild10KB(Path.Combine(RootPath, @"\CSRG\10K\"));
            for (int i = 0; i < 10; i++)
                DRBG800Build10KB(Path.Combine(RootPath, @"\800\10K\"));
            for (int i = 0; i < 10; i++)
                ROrgBuild10KB(Path.Combine(RootPath, @"\ROrg\10K\"));
        }

        /// <summary>
        /// Build the three test sample groups at once
        /// </summary>
        public static void Build100KBx10(string RootPath)
        {
            for (int i = 0; i < 10; i++)
                CSRGBuild100KB(Path.Combine(RootPath, @"\CSRG\100K\"));
            for (int i = 0; i < 10; i++)
                DRBG800Build100KB(Path.Combine(RootPath, @"\800\100K\"));
            for (int i = 0; i < 10; i++)
                ROrgBuild100KB(Path.Combine(RootPath, @"\ROrg\100K\"));
        }
        #endregion
        #endregion
    }
}
