﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Reflection;
using System.IO;

namespace Drbg_Test
{
    public partial class frmHelp : Form
    {
        public frmHelp()
        {
            InitializeComponent();
        }

        private void OnFormLoad(object sender, EventArgs e)
        {

            string filePath = Path.Combine(Application.StartupPath, "CSRG.rtf");
            
            if (File.Exists(filePath))
                rtbHelp.Rtf = System.IO.File.ReadAllText(filePath);

            rtbHelp.SelectAll();
            rtbHelp.SelectionIndent += 12;
            rtbHelp.SelectionRightIndent += 12;
            rtbHelp.SelectionLength = 0;
            rtbHelp.SelectionBackColor = rtbHelp.BackColor;
        }
    }
}
